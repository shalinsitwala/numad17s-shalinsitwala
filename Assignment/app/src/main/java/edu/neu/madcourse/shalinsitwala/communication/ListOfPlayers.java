package edu.neu.madcourse.shalinsitwala.communication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;
import java.util.Vector;

import edu.neu.madcourse.shalinsitwala.R;
import edu.neu.madcourse.shalinsitwala.fcm.FCMActivity;

public class ListOfPlayers extends AppCompatActivity {
    private BroadcastReceiver broadcastReceiver;

    ClassicSingleton cs = ClassicSingleton.getInstance();

    Button btnRegister;
    EditText etEmail;
    String token;
    ArrayList<String> emails = new ArrayList<>();
    String acceptor;

    ListView lvEmails;
    DatabaseReference mDataBaseRef;
    Vector<User> availUsers = new Vector<User>();
    Boolean hasInternet;
    TextView tapOnPlayerText;
    Button btnPlaySinglePlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.com_activity_list_of_players);


        setTitle("Communication");
        checkInternetConnectivity();
        hasInternet = haveNetworkConnection();
        mDataBaseRef = FirebaseDatabase.getInstance().getReference();
        btnRegister = (Button) findViewById(R.id.btnRegister);
        lvEmails = (ListView) findViewById(R.id.lvEmails);
        token = FirebaseInstanceId.getInstance().getToken();
        emails.clear();


        if (hasInternet) {


            tapOnPlayerText = (TextView) findViewById(R.id.tapOnPlayerText);
            tapOnPlayerText.setVisibility(View.VISIBLE);
            btnPlaySinglePlayer = (Button) findViewById(R.id.btnPlaySinglePlayer);
            btnPlaySinglePlayer.setVisibility(View.GONE);

            AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
            AlertDialog dialog;

            View mView = getLayoutInflater().inflate(R.layout.dialog_register, null);
            mBuilder.setView(mView);
            dialog = mBuilder.create();
            dialog.setCanceledOnTouchOutside(false);

            final Button btnRegister = (Button) mView.findViewById(R.id.btnRegister);
            etEmail = (EditText) mView.findViewById(R.id.etEmail);


            final AlertDialog finalDialog = dialog;
            mDataBaseRef.child("users").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.hasChild(token)) {
                        System.out.println("User already there in database.");
                    } else {
                        // pop up the dialog only if this token is not there in the database.

                        finalDialog.show();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            btnRegister.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!etEmail.getText().toString().isEmpty()) {
                        //user object
                        User user = new User(etEmail.getText().toString(), token);

                        //add to database
                        DatabaseReference usersRef = mDataBaseRef.child("users");
                        //token is the key here
                        usersRef.child(token).setValue(user);
                        etEmail.setText("");
                        finalDialog.dismiss();
                        finalDialog.dismiss();
                    }
                }
            });


            setupLayoutInternetFound();

            // get the token of users
        } else {
            // no internet
            hideStuffWhenInternetNotFound();

        }
    }

    protected void setupLayoutInternetFound() {
        tapOnPlayerText = (TextView) findViewById(R.id.tapOnPlayerText);
        tapOnPlayerText.setVisibility(View.VISIBLE);
        btnPlaySinglePlayer = (Button) findViewById(R.id.btnPlaySinglePlayer);
        btnPlaySinglePlayer.setVisibility(View.GONE);

        emails.clear();

        // get data for the listview
        this.retrieveData();
    }


    //Retrieve
    public void retrieveData() {

        mDataBaseRef.child("users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                getUpdatedList(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void getUpdatedList(DataSnapshot ds) {
        emails.clear();
        for (DataSnapshot data : ds.getChildren()) {
            User us = data.getValue(User.class);
            cs.dbUsers.add(us);
        }

        for (User u : cs.dbUsers) {
            if (!u.getToken().equalsIgnoreCase(token)) {
                //if token does not exist in the db
                //add it to ListView to show
                if (!emails.contains(u.getEmail())) {
                    availUsers.add(u);
                    emails.add(u.getEmail());
                }

            }
        }

        // availusers is a vector having the user object except for the current user.

        if (emails.size() > 0) {
            ArrayAdapter adapter = new ArrayAdapter(ListOfPlayers.this, android.R.layout.simple_list_item_1, emails);
            lvEmails.setAdapter(adapter);

            // making it clickable here
            lvEmails.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    Toast.makeText(getBaseContext(),availUsers.elementAt(position).getEmail() + " is selected",Toast.LENGTH_SHORT).show();
                    //Toast.makeText(getBaseContext(), "My token is: " + token + " Opponent's token is: " + availUsers.elementAt(position).getToken(), Toast.LENGTH_SHORT).show();

                    cs.myToken = token;
                    cs.oppToken = availUsers.elementAt(position).getToken();
                    cs.acceptor = availUsers.elementAt(position).getToken();
                    acceptor = availUsers.elementAt(position).getToken();
                    cs.requestor = token;


                    //register a game between these 2 players and add to DB
                    Game game = new Game(token, availUsers.elementAt(position).getToken(), 0, 0, new ArrayList<String>(), new Date());
                    DatabaseReference gamesRef = mDataBaseRef.child("games");
                    gamesRef.push().setValue(game);

                    // SEND NOTIFICATION TO OPPONENT!
                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            pushNotification("Someone challenged you!");
                        }
                    }).start();


                    Intent intent = new Intent(ListOfPlayers.this, GamePlay.class);
                    intent.putExtra("requestor", token);
//                    intent.putExtra("oppToken", availUsers.elementAt(position).getToken());
                    startActivity(intent);
                }
            });


        } else {
            Toast.makeText(ListOfPlayers.this, "No other users. Please try later.", Toast.LENGTH_SHORT).show();
        }
    }


    private void pushNotification(String body) {
        JSONObject jPayload = new JSONObject();
        JSONObject jNotification = new JSONObject();
        JSONObject jData = new JSONObject();
        try {
            // this content is when app is in background
            jNotification.put("title", "Scroggle!");
            jNotification.put("body", body);
            jNotification.put("sound", "default");
            jNotification.put("badge", "1");
            jNotification.put("click_action", "GAMEPLAY");

            jData.put("requestor", token);
//            jData.put("acceptor", acceptor);


            // If sending to a single client
            jPayload.put("to", cs.oppToken);

            /*
            // If sending to multiple clients (must be more than 1 and less than 1000)
            JSONArray ja = new JSONArray();
            ja.put(CLIENT_REGISTRATION_TOKEN);
            // Add Other client tokens
            ja.put(FirebaseInstanceId.getInstance().getToken());
            jPayload.put("registration_ids", ja);
            */

            jPayload.put("priority", "high");
            jPayload.put("notification", jNotification);
            jPayload.put("data", jData);

            URL url = new URL("https://fcm.googleapis.com/fcm/send");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", cs.SERVER_KEY);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);

            // Send FCM message content.
            OutputStream outputStream = conn.getOutputStream();
            outputStream.write(jPayload.toString().getBytes());
            outputStream.close();

            // Read FCM response.
            InputStream inputStream = conn.getInputStream();
            final String resp = convertStreamToString(inputStream);

            Handler h = new Handler(Looper.getMainLooper());
            h.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getBaseContext(), resp, Toast.LENGTH_LONG);
                }
            });
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    private String convertStreamToString(InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next().replace(",", ",\n") : "";
    }

    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }


    //checks continuously
    private void checkInternetConnectivity() {
        IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int[] type = {ConnectivityManager.TYPE_WIFI, ConnectivityManager.TYPE_MOBILE};
                if (CheckInternetBroadcast.isNetworkAvailable(context, type)) {
                    setupLayoutInternetFound();
                } else {
                    Toast.makeText(ListOfPlayers.this, "Please connect to the internet", Toast.LENGTH_SHORT).show();
                    hideStuffWhenInternetNotFound();
                }
            }
        };

        //register receiver
        registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

    protected void hideStuffWhenInternetNotFound() {
        emails.clear();
        emails.add("Please try again with an active Internet connection.");
        emails.add("Meanwhile, you can play the single player game!");
        ArrayAdapter adapter = new ArrayAdapter(ListOfPlayers.this, android.R.layout.simple_list_item_1, emails);
        lvEmails.setAdapter(adapter);

        tapOnPlayerText = (TextView) findViewById(R.id.tapOnPlayerText);
        tapOnPlayerText.setVisibility(View.INVISIBLE);

        btnPlaySinglePlayer = (Button) findViewById(R.id.btnPlaySinglePlayer);
        btnPlaySinglePlayer.setVisibility(View.VISIBLE);

        btnPlaySinglePlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListOfPlayers.this, edu.neu.madcourse.shalinsitwala.wordgame.MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
