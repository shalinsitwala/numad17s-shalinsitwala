package edu.neu.madcourse.shalinsitwala.checkitout;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import edu.neu.madcourse.shalinsitwala.R;

public class RegisterComplete extends AppCompatActivity {

    ImageButton btn_skipnext;
    ImageButton btn_addFriends;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cio_activity_register_complete);


        // hiding bar
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.hide();
        }

        btn_skipnext = (ImageButton) findViewById(R.id.next_button);
        btn_addFriends = (ImageButton) findViewById(R.id.cio_add_friends);


        btn_skipnext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), CheckIn.class);
                startActivity(intent);
            }
        });

        btn_addFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), AddFriend.class);
                startActivity(intent);
                finish();
            }
        });

    }
}
