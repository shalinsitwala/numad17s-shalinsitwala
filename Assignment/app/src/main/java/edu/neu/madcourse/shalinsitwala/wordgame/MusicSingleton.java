package edu.neu.madcourse.shalinsitwala.wordgame;


public class MusicSingleton {

    boolean playMusic = true;


    private static MusicSingleton instance = null;

    private MusicSingleton() {
        // Exists only to defeat instantiation.
    }

    public static MusicSingleton getInstance() {
        if (instance == null) {
            instance = new MusicSingleton();
        }
        return instance;
    }
}


