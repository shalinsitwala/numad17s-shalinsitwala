package edu.neu.madcourse.shalinsitwala.wordgame;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import edu.neu.madcourse.shalinsitwala.R;


public class MainActivity extends AppCompatActivity {
    ClassicSingleton cs = ClassicSingleton.getInstance();
    MusicSingleton ms = MusicSingleton.getInstance();
    public MediaPlayer mMediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wgactivity_main);

        setTitle("Word Game");
        mMediaPlayer = MediaPlayer.create(this, R.raw.acousticbreeze);
        mMediaPlayer.setLooping(true);
    }

    @Override
    protected void onResume() {
        if (ms.playMusic) {
            startMusic();
        } else {
            stopMusic();
        }
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
        stopMusic();

    }

    public void startMusic() {

        mMediaPlayer.start();
    }

    public void stopMusic() {

        if (mMediaPlayer.isPlaying()) {

            mMediaPlayer.pause();
        }
    }

    public void toggleMusic(View v) {
        if (ms.playMusic) {

            //if its true, then stop playing
            ms.playMusic = false;
            stopMusic();
        } else {

            // if boolean is false, then start playing
            ms.playMusic = true;
            startMusic();
        }
    }


}
