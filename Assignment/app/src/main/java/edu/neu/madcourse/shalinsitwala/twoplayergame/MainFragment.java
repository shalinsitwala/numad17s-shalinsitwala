package edu.neu.madcourse.shalinsitwala.twoplayergame;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import edu.neu.madcourse.shalinsitwala.R;



public class MainFragment extends Fragment {
    private AlertDialog mDialog;
    ClassicSingleton cs = ClassicSingleton.getInstance();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.twop_fragment_main, container, false);
        // Handle buttons here...
        View aboutButton = rootView.findViewById(R.id.about_button);
        aboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder =
                        new AlertDialog.Builder(getActivity());
                builder.setTitle("Assignment 8 Acknowledgements");
                builder.setMessage(Html.fromHtml(getString(R.string.ack8_text)));
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.ok_label,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface,
                                                int i) {
                                // nothing
                            }
                        });
                mDialog = builder.show();
            }
        });


//        View infoButton = rootView.findViewById(R.id.info_button);
//        infoButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                AlertDialog.Builder builder =
//                        new AlertDialog.Builder(getActivity());
//                builder.setTitle("Instructions");
//                builder.setMessage(Html.fromHtml(getString(R.string.assign5_instructions)));
//                builder.setCancelable(false);
//                builder.setPositiveButton(R.string.ok_label,
//                        new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface,
//                                                int i) {
//                                // nothing
//                            }
//                        });
//                mDialog = builder.show();
//            }
//        });


//        new button
        View newButton = rootView.findViewById(R.id.new_button);
        newButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ClassicSingleton.ResetSingleton();
                ClassicSingleton cs = ClassicSingleton.getInstance();


                //TODO change timer value
                cs.timerVal = 90 * 1000;
                cs.phase = 1;
                Intent intent = new Intent(getActivity(), ListOfPlayers.class);
                getActivity().startActivity(intent);


            }
        });

//        continue button
//        View continueButton = rootView.findViewById(R.id.continue_button);
//        continueButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                if (cs.timerVal == 0) {
////                    cs.timerVal = 90 * 1000;
////                }
//                Intent intent = new Intent(getActivity(), GameActivity.class);
//                getActivity().startActivity(intent);
//            }
//        });




        return rootView;
    }


}
