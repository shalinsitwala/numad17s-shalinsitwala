package edu.neu.madcourse.shalinsitwala.wordgame;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageButton;

import edu.neu.madcourse.shalinsitwala.*;

public class Tile {


//    private final GameFragment mGame;

    private View mView;
    private Tile mSubTiles[];

    private String text;
    private String status;

//    public Tile(GameFragment game) {
//        this.mGame = game;
//    }

    public Tile() {
    }

    public View getView() {
        return mView;
    }

    public void setView(View view) {
        this.mView = view;
    }


    public Tile[] getSubTiles() {
        return mSubTiles;
    }

    public void setSubTiles(Tile[] subTiles) {
        this.mSubTiles = subTiles;
    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}


