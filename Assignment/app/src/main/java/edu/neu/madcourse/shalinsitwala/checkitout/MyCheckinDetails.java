package edu.neu.madcourse.shalinsitwala.checkitout;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;

import edu.neu.madcourse.shalinsitwala.R;


public class MyCheckinDetails extends AppCompatActivity {

    ClassicSingleton cs = ClassicSingleton.getInstance();
    DatabaseReference mRoofRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference personsRef = mRoofRef.child("persons");
    DatabaseReference thisPersonRef;
    DatabaseReference placesRef = mRoofRef.child("places");
    DatabaseReference thisPlaceRef;
    String token = "";
    Person thisPerson;
    CheckinDetails thisPlace;


    ArrayList<CheckinDetails> checkins;
    ArrayList<String> fAnswers = new ArrayList<>();
    TextView tv_locationname;
    TextView tv_question;
    TextView tv_label_question;
    TextView tv_label_answer;
    EditText et_answer;
    ImageButton btn_next;
    ListView lv_friends_answers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cio_activity_check_in_answer);

        // hiding bar
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.hide();
        }

        token = FirebaseInstanceId.getInstance().getToken();
        thisPersonRef = personsRef.child(token);


        tv_locationname = (TextView) findViewById(R.id.cio_answer_page_who_and_where);
        tv_question = (TextView) findViewById(R.id.cio_answer_question);
        tv_label_question = (TextView) findViewById(R.id.cio_answer_question_label);
        tv_label_answer = (TextView) findViewById(R.id.cio_answer_answer_label);
        et_answer = (EditText) findViewById(R.id.cio_answer_answer);
        btn_next = (ImageButton) findViewById(R.id.next_button);
        lv_friends_answers = (ListView) findViewById(R.id.cio_others_answers);

        thisPersonRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot ds) {
                thisPerson = ds.getValue(Person.class);


                if (thisPerson.getCheckins() != null) {
                    checkins = thisPerson.getCheckins();
                    cs.points = thisPerson.getPoints();

                    thisPlace = checkins.get(thisPerson.getPosInFB());

                    if (thisPlace.getFanswers() != null) {
                        // if there are answers
                        fAnswers.clear();
                        fAnswers = thisPlace.getFanswers();
                    } else {
                        // if no friend has answered yet.
                        fAnswers.clear();
                        fAnswers.add("No answers yet");
                    }
                    ArrayAdapter adapter = new ArrayAdapter(MyCheckinDetails.this, android.R.layout.simple_list_item_1, fAnswers);
                    lv_friends_answers.setAdapter(adapter);

                    if (thisPlace.getAnswer() != null) {
                        cs.oldAnswer = thisPlace.getAnswer();
                    }

                    tv_locationname.setText(thisPlace.getLocationName());

                    if (thisPlace.getQuestion() != null) {
                        // if he had asked a question
                        tv_question.setText(thisPlace.getQuestion());

                        // if there is an answer, fill it in the textbox
                        if (thisPlace.getAnswer() != null) {
                            et_answer.setText(thisPlace.getAnswer());
                        }


                    } else {
                        // if not, then hide stuff too
                        tv_question.setText("You had not asked any question");
                        tv_label_question.setVisibility(View.INVISIBLE);
                        tv_label_answer.setVisibility(View.GONE);
                        et_answer.setVisibility(View.GONE);
                    }


                }

                btn_next.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String answer = et_answer.getText().toString();

                        if (answer.equalsIgnoreCase("") || answer.equalsIgnoreCase(cs.oldAnswer)) {
                            //if no answer, then go to homepage
                            Intent intent = new Intent(getBaseContext(), CheckIn.class);
                            startActivity(intent);
                        } else {
                            // if some new answer , then bonus points

                            thisPersonRef.child("checkins").child(String.valueOf(thisPerson.getPosInFB())).child("answer").setValue(answer);
                            int newPoints = cs.points + 5;
                            thisPersonRef.child("points").setValue(newPoints);
                            //on next button press, take to bonus points page
                            Intent intent = new Intent(getBaseContext(), CheckInAnswerConfirm.class);
                            startActivity(intent);
                        }
                    }
                });


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }
}
