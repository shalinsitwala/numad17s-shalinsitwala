package edu.neu.madcourse.shalinsitwala.checkitout;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;

import edu.neu.madcourse.shalinsitwala.R;

public class AddFriend extends AppCompatActivity {


    DatabaseReference mRoofRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference personsRef = mRoofRef.child("persons");
    DatabaseReference thisPersonRef;
    DatabaseReference placesRef = mRoofRef.child("places");
    String token = "";
    Person thisPerson;
    Friend mfriend;

    ArrayList<Friend> friends;
    ArrayList<Friend> emailsTokensInDB = new ArrayList<>();

    EditText et_friendsEmail;
    ImageButton btn_next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cio_activity_add_friend);
        setTitle("Add Friend");


        et_friendsEmail = (EditText) findViewById(R.id.et_email);
        btn_next = (ImageButton) findViewById(R.id.next_button);

        token = FirebaseInstanceId.getInstance().getToken();
        thisPersonRef = personsRef.child(token);

        thisPersonRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot ds) {
                thisPerson = ds.getValue(Person.class);


                btn_next.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        getAllEmailsInDB();


                        if (isValidEmail(et_friendsEmail.getText())) {
                            mfriend = doesEmailExists(et_friendsEmail.getText().toString());
                            // if valid, then check if exists in the DB
                            if (mfriend.getEmail().equalsIgnoreCase("")) {
                                Toast.makeText(AddFriend.this, "User does not exist", Toast.LENGTH_SHORT).show();
                            } else {
                                // email does exist in the DB
                                if (thisPerson.getFriends() != null) {
                                    friends = thisPerson.getFriends();
                                    Friend friend = new Friend(mfriend.getToken(), et_friendsEmail.getText().toString());
                                    friends.add(friend);
                                    thisPersonRef.child("friends").setValue(friends);

                                } else {
                                    ArrayList<Friend> newFriends = new ArrayList<Friend>();
                                    Friend friend = new Friend(mfriend.getToken(), et_friendsEmail.getText().toString());
                                    newFriends.add(friend);
                                    thisPersonRef.child("friends").setValue(newFriends);
                                }

                                Intent intent = new Intent(getBaseContext(), AddFriendComplete.class);
                                startActivity(intent);
                                finish();
                            }


                        } else {
                            Toast.makeText(AddFriend.this, "Please enter a valid email address.", Toast.LENGTH_SHORT).show();
                        }

                    }
                });


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


    public void getAllEmailsInDB() {

        personsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot ds) {
                for (DataSnapshot d : ds.getChildren()) {
                    Person person = d.getValue(Person.class);

                    if (person.getToken().equalsIgnoreCase(token)) {
                        // don't add this user's id
//                        Toast.makeText(AddFriend.this, "You cannot add yourself as a friend", Toast.LENGTH_SHORT).show();
                    } else {
                        Friend friend = new Friend(person.getToken(), person.getEmail());
                        emailsTokensInDB.add(friend);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public Friend doesEmailExists(String email) {
        // check in emailstokensindb
        for (Friend f : emailsTokensInDB) {
            String emailInArray = f.getEmail();
            if (email.equalsIgnoreCase(emailInArray)) {
                return f;
            }

        }

        return new Friend();
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
}
