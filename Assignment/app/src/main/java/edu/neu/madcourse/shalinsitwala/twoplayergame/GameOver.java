package edu.neu.madcourse.shalinsitwala.twoplayergame;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import edu.neu.madcourse.shalinsitwala.R;


public class GameOver extends Fragment {
    ClassicSingleton cs = ClassicSingleton.getInstance();
    DatabaseReference mRoofRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference gameRef = mRoofRef.child("games");
    Game thisGame = new Game();
    DatabaseReference gameReference;
    String winner = "";
    String textToDisplay = "";
    TextView gameOverText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gameReference = gameRef.child(cs.gameKey);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.twop_fragment_game_over, container, false);
        initViews(rootView);

        return rootView;

    }

    public void initViews(View v) {
        gameOverText = (TextView) v.findViewById(R.id.tvGameOver);
        gameOverText.setTextColor(Color.WHITE);


        gameReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot ds) {
                thisGame = ds.getValue(Game.class);
                int p1score = thisGame.getRscore();
                int p2score = thisGame.getAscore();

                if (p1score == p2score) {
                    textToDisplay = "The game was a Tie!";
                } else if (p1score > p2score) {
                    textToDisplay = "Player1 " + thisGame.getrEmail() + " wins with score " + p1score;
                } else if (p2score > p1score) {
                    textToDisplay = "Player2 " + thisGame.getaEmail() + " wins with score " + p2score;
                }
                gameOverText.setText(textToDisplay);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    public GameOver() {
        // Required empty public constructor
    }


}
