package edu.neu.madcourse.shalinsitwala.communication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import edu.neu.madcourse.shalinsitwala.R;

public class HighScores extends AppCompatActivity {


    private BroadcastReceiver broadcastReceiver;
    ClassicSingleton cs = ClassicSingleton.getInstance();

    DatabaseReference mRoofRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference gamesRef = mRoofRef.child("games");
    DatabaseReference usersRef = mRoofRef.child("users");

    Game game = new Game();
    ArrayList<ScoreTime> top10 = new ArrayList<ScoreTime>();


    ListView lvHighScores;
    Boolean hasInternet;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.com_activity_high_scores);
        setTitle("Top 10 Scores");

        checkInternetConnectivity();
        hasInternet = haveNetworkConnection();

        lvHighScores = (ListView) findViewById(R.id.lvhighScores);
        if (hasInternet) {

            showWhenNet();

        } else {
            //no internet
            hideWhenNoNet();
        }
    }

    public void showWhenNet() {
        gamesRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                top10.clear();
                getAllScores(dataSnapshot);

                Collections.sort(top10, new Comparator<ScoreTime>() {
                    public int compare(ScoreTime o1, ScoreTime o2) {
                        if (o1.score == o2.score)
                            return 0;
                        return o1.score < o2.score ? -1 : 1;
                    }
                });


                Collections.reverse(top10);


                int upto = 0;
                if (top10.size() >= 10) {
                    upto = 10;
                } else {
                    upto = top10.size();
                }

                top10 = new ArrayList<ScoreTime>(top10.subList(0, upto));
                ArrayAdapter<ScoreTime> adapter = new ArrayAdapter<ScoreTime>(HighScores.this, android.R.layout.simple_list_item_1, top10);
                lvHighScores.setAdapter(adapter);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void hideWhenNoNet() {
        ArrayList<String> textWhenNoNet = new ArrayList<String>();
        textWhenNoNet.add("Cannot retrieve High Scores without an internet connection.");
        textWhenNoNet.add("Please connect your phone to Mobile Data or WiFi");

        ArrayAdapter adapter = new ArrayAdapter(HighScores.this, android.R.layout.simple_list_item_1, textWhenNoNet);
        lvHighScores.setAdapter(adapter);
    }


    private void getAllScores(DataSnapshot ds) {
        for (DataSnapshot d : ds.getChildren()) {
            game = d.getValue(Game.class);
            top10.add(new ScoreTime(game.getAscore(), game.getTime()));
            top10.add(new ScoreTime(game.getRscore(), game.getTime()));
        }
    }

    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    private void checkInternetConnectivity() {
        IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int[] type = {ConnectivityManager.TYPE_WIFI, ConnectivityManager.TYPE_MOBILE};
                if (CheckInternetBroadcast.isNetworkAvailable(context, type)) {
                    top10.clear();
                    showWhenNet();
                } else {
                    Toast.makeText(HighScores.this, "Please connect to the internet for updated list", Toast.LENGTH_SHORT).show();
                }
            }
        };

        //register receiver
        registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }



}

class ScoreTime {

    int score;
    Date time;

    public ScoreTime(int score, Date time) {
        this.score = score;
        this.time = time;
    }


    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    @Override
    public String toString() {

        return this.score + " : " + new SimpleDateFormat("yyyy-MM-dd HH:mm").format(this.getTime());
    }


}

