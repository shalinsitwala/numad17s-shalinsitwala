package edu.neu.madcourse.shalinsitwala;

import android.*;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import java.util.Locale;


public class MainActivity extends AppCompatActivity {

    public final static String TitleBarName = "Shalin Sitwala";
    edu.neu.madcourse.shalinsitwala.trickpart.ClassicSingleton cs = edu.neu.madcourse.shalinsitwala.trickpart.ClassicSingleton.getInstance();
    edu.neu.madcourse.shalinsitwala.checkitout.ClassicSingleton csProject = edu.neu.madcourse.shalinsitwala.checkitout.ClassicSingleton.getInstance();

    Geocoder geocoder;
    LocationListener locationListener;
    LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle(TitleBarName);
    }

    public void showAboutDetails(View view) {
        Intent intent = new Intent(this, DisplayAboutMeDetails.class);
        startActivity(intent);
    }

    public void quitApp(View v) {
        System.exit(1);
    }

    public void genError() {

        this.genError();
    }

    public void goToTTTMainMenu(View v) {
        Intent intent = new Intent(this, TTTMainActivity.class);
        startActivity(intent);
    }

    public void goToDictMenu(View v) {
        Intent intent = new Intent(this, TestDictionary.class);
        startActivity(intent);
    }

    public void goToWordGame(View v) {
        Intent intent = new Intent(this, edu.neu.madcourse.shalinsitwala.wordgame.MainActivity.class);
        startActivity(intent);
    }


    public void openCommunication(View v) {
        Intent intent = new Intent(this, edu.neu.madcourse.shalinsitwala.communication.ComHome.class);
        startActivity(intent);
    }

    public void openTwoPlayerGame(View v) {
        Intent intent = new Intent(this, edu.neu.madcourse.shalinsitwala.twoplayergame.MainActivity.class);
        startActivity(intent);
    }

    public void goToTrickPart(View v) {
        geocoder = new Geocoder(this, Locale.getDefault());
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.

                cs.lat = location.getLatitude();
                cs.lon = location.getLongitude();

            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
//                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                startActivity(i);
            }
        };
        configure_button();
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);


        Intent intent = new Intent(this, edu.neu.madcourse.shalinsitwala.trickpart.HomePage.class);
        startActivity(intent);
    }


    void configure_button() {
        // first check for permissions
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.INTERNET}
                        , 10);
            }
            return;
        }
        // this code won't execute IF permissions are not allowed, because in the line above there is return statement.
        locationManager.requestLocationUpdates("gps", 5000, 0, locationListener);
    }


    void goToFinalProject(View v) {
        geocoder = new Geocoder(this, Locale.getDefault());
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.

                csProject.lat = location.getLatitude();
                csProject.lon = location.getLongitude();

            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
//                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                startActivity(i);
            }
        };
        configure_button();
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);


        Intent intent = new Intent(this, edu.neu.madcourse.shalinsitwala.checkitout.MainActivity.class);
        startActivity(intent);
    }


}
