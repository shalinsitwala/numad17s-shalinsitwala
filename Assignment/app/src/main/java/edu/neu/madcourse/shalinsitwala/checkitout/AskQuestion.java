package edu.neu.madcourse.shalinsitwala.checkitout;

import android.content.Intent;
import android.media.Image;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

import edu.neu.madcourse.shalinsitwala.R;

public class AskQuestion extends AppCompatActivity {

    ClassicSingleton cs = ClassicSingleton.getInstance();

    DatabaseReference mRoofRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference personsRef = mRoofRef.child("persons");
    DatabaseReference thisPersonRef;
    DatabaseReference placesRef = mRoofRef.child("places");
    DatabaseReference thisPlaceRef;
    DatabaseReference askingToPersonRef;
    String token = "";
    Person thisPerson;
    Person askingToPerson;

    CheckinDetails thisCheckIn;
    DatabaseReference thisCheckInRef;
    ArrayList<CheckinDetails> checkins;
    ArrayList<String> allTokensInDB = new ArrayList<>();

    EditText et_question;
    ImageButton btn_next;

    int checkInNumber;


//    String pixelToken = "fEGeQ-5nPeY:APA91bEL8R8FLs1ApdbnVIr7JoAU9gLYZ3wi-BwOJxxAFl5jsaE-fBBUz5IHQiVKLszlu9vCYpcplrbh7rNGL7w7KnoVz2NxQbslroUU6_eb_MR0a079pyfFFWEUQT_cz2FRXP6T7Efk";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cio_activity_ask_question);


        // hiding bar
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.hide();
        }

        et_question = (EditText) findViewById(R.id.cio_ask_question_other);
        btn_next = (ImageButton) findViewById(R.id.next_button);


        token = FirebaseInstanceId.getInstance().getToken();
        thisPersonRef = personsRef.child(token);

        getAllTokensOfDB();


        thisPersonRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot ds) {
                thisPerson = ds.getValue(Person.class);

                checkins = thisPerson.getCheckins();

                checkInNumber = checkins.size() - 1;
                // Most recent checkin
                thisCheckIn = checkins.get(checkins.size() - 1);
                thisCheckInRef = thisPersonRef.child("checkins").child(String.valueOf(checkins.size() - 1));

                btn_next.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (et_question.getText().toString().equalsIgnoreCase("")) {
                            Toast.makeText(AskQuestion.this, "Please enter a question", Toast.LENGTH_SHORT).show();

                        } else {


                            thisCheckInRef.child("question").setValue(et_question.getText().toString());
                            // Added to checkins of a user. Now add to places
                            placesRef.child(thisCheckIn.getId()).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    CheckinDetails cd = dataSnapshot.getValue(CheckinDetails.class);
                                    ArrayList<QnA> qna = new ArrayList<QnA>();

                                    if (cd.getQna() != null) {
                                        qna = cd.getQna();
                                    }

                                    QnA qa = new QnA(et_question.getText().toString());
                                    qna.add(qa);
                                    placesRef.child(thisCheckIn.getId()).child("qna").setValue(qna);
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });

                            if (allTokensInDB != null) {
                                for (final String eachToken : allTokensInDB) {

                                    askingToPersonRef = personsRef.child(eachToken);
                                    askingToPersonRef.child("askerToken").setValue(token);
                                    askingToPersonRef.child("askerCheckin").setValue(checkInNumber);

                                    // SEND NOTIFICATION TO FRIEND
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {

                                            pushNotification(thisPerson.getEmail() + " asked a question.", eachToken);
                                        }
                                    }).start();

                                }
                            } else {
//                                Toast.makeText(AskQuestion.this, "array is null", Toast.LENGTH_SHORT).show();
                            }

                            int points = thisPerson.getPoints();
                            points = points - 15;
                            thisPersonRef.child("points").setValue(points);

                            Intent intent = new Intent(getBaseContext(), SendQuestionConfirmation.class);
                            startActivity(intent);
                            finish();

                        }
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private void pushNotification(String body, String to) {
        JSONObject jPayload = new JSONObject();
        JSONObject jNotification = new JSONObject();
        JSONObject jData = new JSONObject();
        try {
            // this content is when app is in background
            jNotification.put("title", "Check It Out");
            jNotification.put("body", body);
            jNotification.put("sound", "default");
            jNotification.put("badge", "1");
            jNotification.put("click_action", "ANSWERPAGE");

            // If sending to a single client
            jPayload.put("to", to);
            jPayload.put("priority", "high");
            jPayload.put("notification", jNotification);
            jPayload.put("data", jData);

            URL url = new URL("https://fcm.googleapis.com/fcm/send");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", cs.SERVER_KEY);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);

            // Send FCM message content.
            OutputStream outputStream = conn.getOutputStream();
            outputStream.write(jPayload.toString().getBytes());
            outputStream.close();

            // Read FCM response.
            InputStream inputStream = conn.getInputStream();
            final String resp = convertStreamToString(inputStream);

            Handler h = new Handler(Looper.getMainLooper());
            h.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getBaseContext(), resp, Toast.LENGTH_LONG);
                }
            });
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    private String convertStreamToString(InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next().replace(",", ",\n") : "";
    }


    public void getAllTokensOfDB() {
        personsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot ds) {
                for (DataSnapshot d : ds.getChildren()) {
                    Person person = d.getValue(Person.class);
                    if (person.getToken().equalsIgnoreCase(token)) {
                        // don't add this user's id

                    } else {
                        allTokensInDB.add(person.getToken());
//                        Toast.makeText(AskQuestion.this, "Token added" + person.getToken(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
