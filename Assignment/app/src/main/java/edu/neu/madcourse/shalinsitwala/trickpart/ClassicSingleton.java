package edu.neu.madcourse.shalinsitwala.trickpart;

public class ClassicSingleton {

    public double lat = 42.3393519;
    public double lon = -71.0903621;
    double neulat = -71.0903621;
    double neulon = 42.3393519;


    private static ClassicSingleton instance = null;

    private ClassicSingleton() {
        // Exists only to defeat instantiation.
    }

    public static ClassicSingleton getInstance() {
        if (instance == null) {
            instance = new ClassicSingleton();

        }
        return instance;
    }
}