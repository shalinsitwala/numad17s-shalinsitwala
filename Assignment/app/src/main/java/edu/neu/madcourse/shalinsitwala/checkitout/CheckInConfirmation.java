package edu.neu.madcourse.shalinsitwala.checkitout;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

import edu.neu.madcourse.shalinsitwala.R;


public class CheckInConfirmation extends AppCompatActivity {

    ClassicSingleton cs = ClassicSingleton.getInstance();

    DatabaseReference mRoofRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference personsRef = mRoofRef.child("persons");
    DatabaseReference thisPersonRef;
    DatabaseReference placesRef = mRoofRef.child("places");
    DatabaseReference thisPlaceRef;
    String token = "";
    Person thisPerson;
    CheckinDetails checkinDetails;

    ArrayList<CheckinDetails> checkins;
    ArrayList<String> topQA = new ArrayList<>();

    TextView tv_locationName;
    ListView lv_topQuestions;
    ImageButton btn_next;
    Button btn_askquestion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cio_activity_check_in_confirmation);



        // hiding bar
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.hide();
        }


        tv_locationName = (TextView) findViewById(R.id.checkinplace);
        lv_topQuestions = (ListView) findViewById(R.id.questionsananswers);
        btn_next = (ImageButton) findViewById(R.id.next_button);
        btn_askquestion = (Button) findViewById(R.id.askaquestion);


        token = FirebaseInstanceId.getInstance().getToken();
        thisPersonRef = personsRef.child(token);

        thisPersonRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot ds) {
                thisPerson = ds.getValue(Person.class);

                checkins = thisPerson.getCheckins();

                if (checkins.size() < 5) {
                    btn_askquestion.setVisibility(View.INVISIBLE);
                } else {
                    btn_askquestion.setVisibility(View.VISIBLE);
                }

                btn_askquestion.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int points = thisPerson.getPoints();
                        if (points < 15) {
                            Toast.makeText(CheckInConfirmation.this, "Insufficient points.", Toast.LENGTH_SHORT).show();
                        } else {
                            Intent intent = new Intent(getBaseContext(), CheckInQuestion.class);
                            startActivity(intent);
                            finish();
                        }
                    }
                });


                //Most recent checkin
                checkinDetails = checkins.get(checkins.size() - 1);
                tv_locationName.setText(checkinDetails.getLocationName());

                String placeId = checkinDetails.getId();

                thisPlaceRef = placesRef.child(placeId);
                thisPlaceRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot ds) {
                        CheckinDetails thisPlace = ds.getValue(CheckinDetails.class);

                        if (thisPlace != null && thisPlace.getQna() != null) {
                            ArrayList<QnA> qna = thisPlace.getQna();


                            getOneAnswerPerQuestionToString(qna);

                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(CheckInConfirmation.this, android.R.layout.simple_list_item_1, topQA);
                            lv_topQuestions.setAdapter(adapter);
                        } else {
                            // if no qna in "places"
                            topQA.clear();
                            topQA.add("This location does not have popular questions");
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(CheckInConfirmation.this, android.R.layout.simple_list_item_1, topQA);
                            lv_topQuestions.setAdapter(adapter);
                        }


                        btn_next.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if (checkins.size() == 1) {
                                    // go to tutorial for mycheckins
                                    Intent intent = new Intent(getBaseContext(), tutorial_mycheckins.class);
                                    startActivity(intent);
                                } else if (checkins.size() == 5) {
                                    // go to register tutorial
                                    Intent intent = new Intent(getBaseContext(), tutorial_register.class);
                                    startActivity(intent);
                                } else {
                                    Intent intent = new Intent(getBaseContext(), CheckIn.class);
                                    startActivity(intent);
                                }
                                finish();

                            }
                        });


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });


    }

    public void getOneAnswerPerQuestionToString(ArrayList<QnA> qna) {
        for (QnA qa : qna) {

            if (qa.getAnswers() != null) {
                String q1a = "Q: " + qa.getQuestion() + "\n A: " + qa.getAnswers().get(0);
                topQA.add(q1a);
            } else {
                String q1a = "Q: " + qa.getQuestion() + "\n A: " + "No answer yet";
                topQA.add(q1a);
            }

        }

    }








}
