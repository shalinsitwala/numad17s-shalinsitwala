package edu.neu.madcourse.shalinsitwala.checkitout;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;

import edu.neu.madcourse.shalinsitwala.R;

public class tutorial_askQuestions extends AppCompatActivity {


    DatabaseReference mRoofRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference personsRef = mRoofRef.child("persons");
    DatabaseReference thisPersonRef;
    DatabaseReference placesRef = mRoofRef.child("places");
    DatabaseReference thisPlaceRef;
    String token = "";
    Person thisPerson;
    CheckinDetails checkinDetails;

    ArrayList<CheckinDetails> checkins;

    TextView tv_locationName;
    ImageButton btn_next;
    Button btn_askquestion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cio_activity_tutorial_ask_questions);

        // hiding bar
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.hide();
        }

        tv_locationName = (TextView) findViewById(R.id.checkinplace);
        btn_next = (ImageButton) findViewById(R.id.next_button);
        btn_askquestion = (Button) findViewById(R.id.askaquestion);

        token = FirebaseInstanceId.getInstance().getToken();
        thisPersonRef = personsRef.child(token);

        thisPersonRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot ds) {
                thisPerson = ds.getValue(Person.class);

                checkins = thisPerson.getCheckins();

                //Most recent checkin
                checkinDetails = checkins.get(checkins.size() - 1);
                tv_locationName.setText(checkinDetails.getLocationName());


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        btn_askquestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), CheckInQuestion.class);
                startActivity(intent);
                finish();
            }
        });


        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), CheckInConfirmation.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
