package edu.neu.madcourse.shalinsitwala.checkitout;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;

import edu.neu.madcourse.shalinsitwala.R;

public class MainActivity extends AppCompatActivity {
    AlertDialog mDialog;
    Button btn_ack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cio_activity_main);


        setTitle("Final Project");


        btn_ack = (Button) findViewById(R.id.button5);

        btn_ack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Final Project Acknowledgements");
                builder.setMessage(Html.fromHtml(getString(R.string.ack_project)));
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.ok_label,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface,
                                                int i) {
                                // nothing
                            }
                        });
                mDialog = builder.show();
            }
        });


    }


    public void startProject(View v) {
        Intent intent = new Intent(this, LandingPage.class);
        startActivity(intent);
    }
}
