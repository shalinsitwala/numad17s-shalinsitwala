package edu.neu.madcourse.shalinsitwala.twoplayergame;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import edu.neu.madcourse.shalinsitwala.R;
import edu.neu.madcourse.shalinsitwala.communication.*;


public class HighScores extends AppCompatActivity {
    private BroadcastReceiver broadcastReceiver;


    ClassicSingleton cs = ClassicSingleton.getInstance();

    DatabaseReference mRoofRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference gamesRef = mRoofRef.child("games");
    Game game = new Game();
    ArrayList<ScoreUserTime> top10 = new ArrayList<ScoreUserTime>();
    ListView lvHighScores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.twop_activity_high_scores);
        setTitle("Top 10 Scores");

        checkInternetConnectivity();

        lvHighScores = (ListView) findViewById(R.id.lvhighScores);

    }


    public void showWhenNet() {
        gamesRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                top10.clear();
                getAllScores(dataSnapshot);

                Collections.sort(top10, new Comparator<ScoreUserTime>() {
                    public int compare(ScoreUserTime o1, ScoreUserTime o2) {
                        if (o1.score == o2.score)
                            return 0;
                        return o1.score < o2.score ? -1 : 1;
                    }
                });


                Collections.reverse(top10);


                int upto = 0;
                if (top10.size() >= 10) {
                    upto = 10;
                } else {
                    upto = top10.size();
                }

                top10 = new ArrayList<ScoreUserTime>(top10.subList(0, upto));
                ArrayAdapter<ScoreUserTime> adapter = new ArrayAdapter<ScoreUserTime>(HighScores.this, android.R.layout.simple_list_item_1, top10);
                lvHighScores.setAdapter(adapter);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    public void hideWhenNoNet() {
        ArrayList<String> textWhenNoNet = new ArrayList<String>();
        textWhenNoNet.add("Cannot retrieve High Scores without an internet connection.");
        textWhenNoNet.add("Please connect your phone to Mobile Data or WiFi");

        ArrayAdapter adapter = new ArrayAdapter(HighScores.this, android.R.layout.simple_list_item_1, textWhenNoNet);
        lvHighScores.setAdapter(adapter);
    }


    private void getAllScores(DataSnapshot ds) {
        for (DataSnapshot d : ds.getChildren()) {
            game = d.getValue(Game.class);
            top10.add(new ScoreUserTime(game.getRscore(), game.getrEmail(), game.getTime()));
            top10.add(new ScoreUserTime(game.getAscore(), game.getaEmail(), game.getTime()));
        }
    }

    private void checkInternetConnectivity() {
        IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int[] type = {ConnectivityManager.TYPE_WIFI, ConnectivityManager.TYPE_MOBILE};
                if (CheckInternetBroadcast.isNetworkAvailable(context, type)) {
                    top10.clear();
                    showWhenNet();
                } else {
                    Toast.makeText(HighScores.this, "Please connect to the internet for updated list", Toast.LENGTH_SHORT).show();
                }
            }
        };

        //register receiver
        registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }


}


class ScoreUserTime {

    int score;
    String email;
    Date time;
//    String country;

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }


    public ScoreUserTime(int score, String email, Date time) {
        this.score = score;
        this.email = email;
        this.time = time;
//        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }


    @Override
    public String toString() {

//        return this.score + " : " + new SimpleDateFormat("yyyy-MM-dd HH:mm").format(this.getTime());

        return this.score + " - " + this.email + " - " + new SimpleDateFormat("yyyy-MM-dd HH:mm").format(this.getTime());
//                + this.country;
    }


}
