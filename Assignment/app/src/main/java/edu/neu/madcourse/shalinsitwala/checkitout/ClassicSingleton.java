package edu.neu.madcourse.shalinsitwala.checkitout;


public class ClassicSingleton {


    int posInFb;
    int points;
    String oldAnswer = "";
    public String SERVER_KEY = "key=AAAAh6WDbO8:APA91bFyv8KyVvapjwjhlOZR3G77IyY4JFEZ8OR_hkkK7MTqCoFuZ7cKiTbU1IVr3f70r_S1-KrP4Cl1RCzakCoKHfjmjQ3KyGoKWsacL-DsM_zhL53hhkCLwOdnqwBOCIQGJqp5WYdv";


    public double lat = 42.3393519;
    public double lon = -71.0903621;
    double neulat = -71.0903621;
    double neulon = 42.3393519;

    private static ClassicSingleton instance = null;

    protected ClassicSingleton() {
        // Exists only to defeat instantiation.
    }

    public static ClassicSingleton getInstance() {
        if (instance == null) {
            instance = new ClassicSingleton();
        }
        return instance;
    }
}