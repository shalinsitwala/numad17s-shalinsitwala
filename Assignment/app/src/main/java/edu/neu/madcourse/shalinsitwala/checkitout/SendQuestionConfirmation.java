package edu.neu.madcourse.shalinsitwala.checkitout;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;

import edu.neu.madcourse.shalinsitwala.R;

public class SendQuestionConfirmation extends AppCompatActivity {
    DatabaseReference mRoofRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference personsRef = mRoofRef.child("persons");
    DatabaseReference thisPersonRef;
    DatabaseReference placesRef = mRoofRef.child("places");
    DatabaseReference thisPlaceRef;
    String token = "";
    Person thisPerson;
    CheckinDetails checkinDetails;
    ArrayList<CheckinDetails> checkins;


    ImageButton btn_next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cio_activity_send_question_confirmation);

        // hiding bar
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.hide();
        }

        btn_next = (ImageButton) findViewById(R.id.next_button);


        token = FirebaseInstanceId.getInstance().getToken();
        thisPersonRef = personsRef.child(token);

        thisPersonRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot ds) {
                thisPerson = ds.getValue(Person.class);

                checkins = thisPerson.getCheckins();

                btn_next.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (checkins.size() == 5) {
                            // go to register tutorial
                            Intent intent = new Intent(getBaseContext(), tutorial_register.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Intent intent = new Intent(getBaseContext(), CheckIn.class);
                            startActivity(intent);
                            finish();
                        }

                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }
}
