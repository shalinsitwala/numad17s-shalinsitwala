package edu.neu.madcourse.shalinsitwala.twoplayergame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import edu.neu.madcourse.shalinsitwala.R;

public class MainActivity extends AppCompatActivity {
    ClassicSingleton cs = ClassicSingleton.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.twop_activity_main);


        setTitle("Two Player Word Game");
    }

//    public void goToLocation(View v) {
//        Intent intent = new Intent(this, GetLocation.class);
//        startActivity(intent);
//    }

    public void goToHighScores(View v) {
        Intent intent = new Intent(this, HighScores.class);
        startActivity(intent);
    }
}
