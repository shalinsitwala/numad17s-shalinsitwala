package edu.neu.madcourse.shalinsitwala;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class TestDictionary extends AppCompatActivity {

    ArrayList<String> displayList = new ArrayList<String>();
    Vector<String> loaded = new Vector<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState != null){
            displayList = savedInstanceState.getStringArrayList("displayList");
        }
        setContentView(R.layout.activity_test_dictionary);
        setTitle("Test Dictionary");


        EditText et = (EditText) findViewById(R.id.enterWord);
        final ListView lv = (ListView) findViewById(R.id.listView);
        final MediaPlayer mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.beep_for_word);




        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
//                mediaPlayer.stop();
                String typed = s.toString();
                typed = typed.toLowerCase();
                if(typed.length()==3){
                    loadFileWords(typed);
                }

                // handle keywords
                if(typed.equals("con") || typed.equals("for") || typed.equals("int")
                        || typed.equals("new") || typed.equals("try")){
                    typed = typed + '1';
                    loadFileWords(typed);

                    if(typed.contains("1")){
                        typed.replace("1","");
                    }
                }



                if(loaded.contains(typed)){

                    if(!displayList.contains(typed)){
                        displayList.add(typed);

                        mediaPlayer.seekTo(0);
                        mediaPlayer.start();

                    }

                }

                displayInTheList();

            }

            void loadFileWords(String typed){
                try{
                    BufferedReader buffer = new BufferedReader(
                            new InputStreamReader(getResources().getAssets().open(typed)));

                    String str_line ="";

                    while ((str_line = buffer.readLine()) != null)
                    {
                        str_line = str_line.trim();
                        if ((str_line.length()!=0))
                        {
                            //if those 3 letters file found, then load all the words of that file
                            // in our vector
                            loaded.add(str_line);
                        }
                    }


                }
                catch (Exception e){
                }
            }

            void displayInTheList(){
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>
                        (TestDictionary.this, android.R.layout.simple_list_item_1, displayList);
                lv.setAdapter(arrayAdapter);

            }
        });

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        savedInstanceState.putStringArrayList("displayList", displayList);
        super.onSaveInstanceState(savedInstanceState);
    }




        public void returnToMM(View v){
        finish();
    }

    public void showAck(View v){
        Intent intent = new Intent(this, showAck.class);
        startActivity(intent);

    }

    public void clear(View v){
        // clearing the enter word textbox
        EditText et = (EditText) findViewById(R.id.enterWord);
        et.setText("");

        //clearing the list displayed
        ListView lv = (ListView) findViewById(R.id.listView);
        lv.setAdapter(null);
        displayList.clear();

    }
}
