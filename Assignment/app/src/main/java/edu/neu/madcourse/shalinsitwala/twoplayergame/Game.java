package edu.neu.madcourse.shalinsitwala.twoplayergame;

import android.content.IntentFilter;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


public class Game {
    private String requestor;
    private String acceptor;
    private int rscore;
    private int ascore;
    private ArrayList<String> awords;
    private ArrayList<String> rwords;
    private String turn;
    private int tilesPopulated;
    private List<List<MyTile>> mSmallTiles = new ArrayList<List<MyTile>>();
    private ArrayList<Integer> lastClickPosition;
    private ArrayList<String> selectedLetters;
    private ArrayList<String> trueWords;
    private List<List<Integer>> selectedPositions = new ArrayList<List<Integer>>();
    private String aEmail;

    public String getaEmail() {
        return aEmail;
    }

    public void setaEmail(String aEmail) {
        this.aEmail = aEmail;
    }

    public String getrEmail() {
        return rEmail;
    }

    public void setrEmail(String rEmail) {
        this.rEmail = rEmail;
    }

    private String rEmail;


    public ArrayList<Integer> getLastClickPosition() {
        return lastClickPosition;
    }

    public void setLastClickPosition(ArrayList<Integer> lastClickPosition) {
        this.lastClickPosition = lastClickPosition;
    }

    public ArrayList<String> getSelectedLetters() {
        return selectedLetters;
    }

    public void setSelectedLetters(ArrayList<String> selectedLetters) {
        this.selectedLetters = selectedLetters;
    }

    public ArrayList<String> getTrueWords() {
        return trueWords;
    }

    public void setTrueWords(ArrayList<String> trueWords) {
        this.trueWords = trueWords;
    }

    public List<List<Integer>> getSelectedPositions() {
        return selectedPositions;
    }

    public void setSelectedPositions(List<List<Integer>> selectedPositions) {
        this.selectedPositions = selectedPositions;
    }


    public List<List<MyTile>> getmSmallTiles() {
        return mSmallTiles;
    }

    public void setmSmallTiles(List<List<MyTile>> mSmallTiles) {
        this.mSmallTiles = mSmallTiles;
    }


    public int getTilesPopulated() {
        return tilesPopulated;
    }

    public void setTilesPopulated(int tilesPopulated) {
        this.tilesPopulated = tilesPopulated;
    }


    public Game(String requestor, String acceptor, int rscore, int ascore, ArrayList<String> awords,
                ArrayList<String> rwords,
                Date time, String turn, ArrayList<Integer> lastClickPosition, ArrayList<String> selectedLetters,
                ArrayList<String> trueWords, String rEmail, String aEmail) {
        this.requestor = requestor;
        this.acceptor = acceptor;
        this.rscore = rscore;
        this.ascore = ascore;
        this.awords = awords;
        this.rwords = rwords;
        this.time = time; //date time
        this.turn = turn; // p1 or p2
        this.tilesPopulated = 0;
        this.lastClickPosition = lastClickPosition;
        this.selectedLetters = selectedLetters;
        this.trueWords = trueWords;
        this.rEmail = rEmail;
        this.aEmail = aEmail;

//        this.selectedPositions = selectedPositions;

    }


    public String getTurn() {
        return turn;
    }

    public void setTurn(String turn) {
        this.turn = turn;
    }

    public ArrayList<String> getAwords() {
        return awords;
    }

    public void setAwords(ArrayList<String> awords) {
        this.awords = awords;
    }

    public ArrayList<String> getRwords() {
        return rwords;
    }

    public void setRwords(ArrayList<String> rwords) {
        this.rwords = rwords;
    }


    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    private Date time;


    public int getRscore() {
        return rscore;
    }

    public void setRscore(int rscore) {
        this.rscore = rscore;
    }

    public int getAscore() {
        return ascore;
    }

    public void setAscore(int ascore) {
        this.ascore = ascore;
    }


    public Game() {
        this.acceptor = "";
        this.requestor = "";
        this.rscore = 0;
        this.ascore = 0;
        this.awords = new ArrayList<String>();
        this.rwords = new ArrayList<String>();
        this.time = new Date();
        this.turn = "a";
        this.tilesPopulated = 0;
        this.lastClickPosition = new ArrayList<Integer>();
        this.selectedLetters = new ArrayList<String>();
        this.trueWords = new ArrayList<String>();
        this.rEmail = "";
        this.aEmail = "";

//        this.selectedPositions = new ArrayList<>();
    }

    public String getAcceptor() {
        return acceptor;
    }

    public void setAcceptor(String acceptor) {
        this.acceptor = acceptor;
    }

    public String getRequestor() {
        return requestor;
    }

    public void setRequestor(String requestor) {
        this.requestor = requestor;
    }
}
