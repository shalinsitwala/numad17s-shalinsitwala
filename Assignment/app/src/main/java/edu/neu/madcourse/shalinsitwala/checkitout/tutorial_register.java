package edu.neu.madcourse.shalinsitwala.checkitout;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import edu.neu.madcourse.shalinsitwala.R;

public class tutorial_register extends AppCompatActivity {


    ImageButton btn_register;
    ImageButton btn_next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cio_activity_tutorial_register);


        // hiding bar
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.hide();
        }

        btn_register = (ImageButton) findViewById(R.id.cio_register_button);
        btn_next = (ImageButton) findViewById(R.id.next_button);


        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), Register.class);
                startActivity(intent);
                finish();
            }
        });


        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), CheckIn.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
