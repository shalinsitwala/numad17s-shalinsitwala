package edu.neu.madcourse.shalinsitwala.checkitout;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import edu.neu.madcourse.shalinsitwala.R;
import edu.neu.madcourse.shalinsitwala.twoplayergame.*;


public class CheckIn extends AppCompatActivity {
    public BroadcastReceiver broadcastReceiver;
    ClassicSingleton cs = ClassicSingleton.getInstance();

    DatabaseReference mRoofRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference personsRef = mRoofRef.child("persons");
    DatabaseReference thisPersonRef;
    DatabaseReference placesRef = mRoofRef.child("places");
    String token = "";
    Person thisPerson;

    TextView tv_points;
    TextView tv_checkins;
    Button btn_checkin;
    Button btn_mycheckins;
    ImageButton btn_register;
    ImageButton btn_addFriends;
    TextView tv_register_label;
    TextView tv_friends_label;
    ImageView iv_arrow;
    TextView tv_tutorial_text;
    TextView tv_goodwork;

    //    LatLngBounds BOUNDS_NEU = new LatLngBounds(new LatLng(42.3398106, -71.0913604), new LatLng(42.3398106 + 0.003245, -71.0913604 + 0.00208741));
    LatLngBounds BOUNDS_NEU = new LatLngBounds(new LatLng(cs.lat, cs.lon), new LatLng(cs.lat + 0.003245, cs.lon + 0.00208741));
    final int PLACE_PICKER_REQUEST = 1;

    Geocoder geocoder;
    LocationListener locationListener;
    LocationManager locationManager;


    ArrayList<String> tokens;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cio_activity_check_in);

        checkInternetConnectivity();

        // hiding bar
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.hide();
        }

        tv_points = (TextView) findViewById(R.id.cio_points);
        tv_checkins = (TextView) findViewById(R.id.cio_checkins);
        btn_checkin = (Button) findViewById(R.id.button8);
        btn_mycheckins = (Button) findViewById(R.id.cio_mycheckins);
        btn_register = (ImageButton) findViewById(R.id.cio_register_button);
        btn_addFriends = (ImageButton) findViewById(R.id.cio_add_friends);
        tv_friends_label = (TextView) findViewById(R.id.cio_add_friends_label);
        tv_register_label = (TextView) findViewById(R.id.cio_register_label);
        iv_arrow = (ImageView) findViewById(R.id.tutorial_arrow);
        tv_tutorial_text = (TextView) findViewById(R.id.tutorial_text);
        tv_goodwork = (TextView) findViewById(R.id.tv_goodwork);


        token = FirebaseInstanceId.getInstance().getToken();
        thisPersonRef = personsRef.child(token);


        thisPersonRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot ds) {


                thisPerson = ds.getValue(Person.class);
                tv_points.setText(String.valueOf(thisPerson.getPoints()));

                if (thisPerson.getCheckins() == null) {
                    btn_register.setVisibility(View.INVISIBLE);
                    btn_addFriends.setVisibility(View.INVISIBLE);
                    tv_friends_label.setVisibility(View.INVISIBLE);
                    tv_register_label.setVisibility(View.INVISIBLE);
                    btn_mycheckins.setVisibility(View.INVISIBLE);
                    iv_arrow.setVisibility(View.VISIBLE);
                    tv_tutorial_text.setVisibility(View.VISIBLE);
                    tv_goodwork.setVisibility(View.INVISIBLE);

                } else if (thisPerson.getCheckins().size() < 5) {
                    btn_register.setVisibility(View.INVISIBLE);
                    btn_addFriends.setVisibility(View.INVISIBLE);
                    tv_friends_label.setVisibility(View.INVISIBLE);
                    tv_register_label.setVisibility(View.INVISIBLE);
                    btn_mycheckins.setVisibility(View.VISIBLE);
                    iv_arrow.setVisibility(View.INVISIBLE);
                    tv_tutorial_text.setVisibility(View.INVISIBLE);
                    tv_checkins.setText(String.valueOf(thisPerson.getCheckins().size()));
                    tv_goodwork.setVisibility(View.VISIBLE);
                    tv_goodwork.setText(5 - thisPerson.getCheckins().size() + " more check-ins to start asking questions.");
                } else {
                    btn_mycheckins.setVisibility(View.VISIBLE);
                    tv_checkins.setText(String.valueOf(thisPerson.getCheckins().size()));
                    iv_arrow.setVisibility(View.INVISIBLE);
                    tv_tutorial_text.setVisibility(View.INVISIBLE);
                    tv_goodwork.setVisibility(View.INVISIBLE);

                    if (thisPerson.getEmail() == null || thisPerson.getEmail().equalsIgnoreCase("")) {
                        btn_register.setVisibility(View.VISIBLE);
                        tv_register_label.setVisibility(View.VISIBLE);
                        btn_addFriends.setVisibility(View.INVISIBLE);
                        tv_friends_label.setVisibility(View.INVISIBLE);
                    } else {
                        btn_register.setVisibility(View.INVISIBLE);
                        tv_register_label.setVisibility(View.INVISIBLE);
                        btn_addFriends.setVisibility(View.VISIBLE);
                        tv_friends_label.setVisibility(View.VISIBLE);
                    }


                }


                btn_checkin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        ////////// LOCATION STARTS///////////
                        geocoder = new Geocoder(getBaseContext(), Locale.getDefault());
                        locationManager = (LocationManager) CheckIn.this.getSystemService(Context.LOCATION_SERVICE);
                        locationListener = new LocationListener() {
                            public void onLocationChanged(Location location) {
                                // Called when a new location is found by the network location provider.

                                cs.lat = location.getLatitude();
                                cs.lon = location.getLongitude();
                                BOUNDS_NEU = new LatLngBounds(new LatLng(cs.lat, cs.lon), new LatLng(cs.lat + 0.003245, cs.lon + 0.00208741));

                            }

                            public void onStatusChanged(String provider, int status, Bundle extras) {
                            }

                            public void onProviderEnabled(String provider) {
                            }

                            public void onProviderDisabled(String provider) {
//                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                startActivity(i);
                            }
                        };
                        configure_button();
                        if (ActivityCompat.checkSelfPermission(getBaseContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getBaseContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);


                        //////////// LOCATION ENDS //////////////


                        try {
                            PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();
                            intentBuilder.setLatLngBounds(BOUNDS_NEU);
                            Intent intent = intentBuilder.build(CheckIn.this);
                            startActivityForResult(intent, PLACE_PICKER_REQUEST);

                        } catch (GooglePlayServicesRepairableException
                                | GooglePlayServicesNotAvailableException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        btn_mycheckins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), MyCheckins.class);
                startActivity(intent);
            }
        });

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), Register.class);
                startActivity(intent);
            }
        });

        btn_addFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), AddFriend.class);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }


    private void checkInternetConnectivity() {
        IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int[] type = {ConnectivityManager.TYPE_WIFI, ConnectivityManager.TYPE_MOBILE};
                if (CheckInternetBroadcast.isNetworkAvailable(context, type)) {
//                    internetFound();

                    btn_checkin.setVisibility(View.VISIBLE);
                    tv_goodwork.setVisibility(View.INVISIBLE);
                } else {
//                    internetGone();
                    btn_checkin.setVisibility(View.INVISIBLE);
                    tv_goodwork.setVisibility(View.VISIBLE);
                    tv_goodwork.setText("Please check your internet connection and try again.");

                }
            }
        };

        //register receiver
        getBaseContext().registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST && resultCode == Activity.RESULT_OK) {
            final Place place = PlacePicker.getPlace(this, data);
            final CharSequence name = place.getName();
            final CharSequence address = place.getAddress();

            String attributions = (String) place.getAttributions();
            if (attributions == null) {
                attributions = "";
            }


            final CheckinDetails checkinDetails = new CheckinDetails(place.getId());

            checkinDetails.setAddress(String.valueOf(address));
            checkinDetails.setLocationName(String.valueOf(name));
            checkinDetails.setId(place.getId());
            checkinDetails.setTime(new Date());


            placesRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot ds) {
                    if (ds.hasChild(place.getId())) {
                    } else {

                        placesRef.child(place.getId()).setValue(checkinDetails);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });


            thisPersonRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot ds) {
                    thisPerson = ds.getValue(Person.class);
                    int points = thisPerson.getPoints();
                    points = points + 10;
                    thisPersonRef.child("points").setValue(points);

                    if (thisPerson.getCheckins() == null) {
                        //If this is the 1st checkin
                        ArrayList<CheckinDetails> checkins = new ArrayList<>();
                        checkins.add(checkinDetails);
                        thisPersonRef.child("checkins").setValue(checkins);

                        Intent intent = new Intent(getBaseContext(), CheckInConfirmation.class);
                        startActivity(intent);

                    } else {

                        ArrayList<CheckinDetails> checkins = thisPerson.getCheckins();
                        checkins.add(checkinDetails);
                        thisPersonRef.child("checkins").setValue(checkins);

                        if (checkins.size() == 5) {
                            Intent intent = new Intent(getBaseContext(), tutorial_askQuestions.class);
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(getBaseContext(), CheckInConfirmation.class);
                            startActivity(intent);
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            });
        }
    }

    void configure_button() {
        // first check for permissions
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.INTERNET}
                        , 10);
            }
            return;
        }
        // this code won't execute IF permissions are not allowed, because in the line above there is return statement.
        locationManager.requestLocationUpdates("gps", 5000, 0, locationListener);
    }


}
