package edu.neu.madcourse.shalinsitwala.twoplayergame;

import android.view.View;

import com.google.firebase.database.Exclude;

import java.util.List;

public class Tile {


    //    private final GameFragment mGame;
    private View mView;
    private Tile mSubTiles[];

//    public List<Tile> getMySubTiles() {
//        return mySubTiles;
//    }
//
//    public void setMySubTiles(List<Tile> mySubTiles) {
//        this.mySubTiles = mySubTiles;
//    }
//
//    private List<Tile> mySubTiles;

    private String text;
    private String status;

//    public Tile(GameFragment game) {
//        this.mGame = game;
//    }

    public Tile() {
    }


    public View getView() {
        return mView;
    }

    public void setView(View view) {
        this.mView = view;
    }

    public Tile[] getSubTiles() {
        return mSubTiles;
    }


    public void setSubTiles(Tile[] subTiles) {

        this.mSubTiles = subTiles;
    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}


