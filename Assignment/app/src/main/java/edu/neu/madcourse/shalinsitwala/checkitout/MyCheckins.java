package edu.neu.madcourse.shalinsitwala.checkitout;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;

import edu.neu.madcourse.shalinsitwala.R;

public class MyCheckins extends AppCompatActivity {

    ClassicSingleton cs = ClassicSingleton.getInstance();
    DatabaseReference mRoofRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference personsRef = mRoofRef.child("persons");
    DatabaseReference thisPersonRef;
    DatabaseReference placesRef = mRoofRef.child("places");
    DatabaseReference thisPlaceRef;
    String token = "";
    Person thisPerson;
    CheckinDetails checkinDetails;


    ArrayList<String> myCheckins = new ArrayList<>();
    ArrayList<CheckinDetails> checkins;


    ListView lv_mycheckins;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cio_activity_my_checkins);
        setTitle("My Checkins");

        lv_mycheckins = (ListView) findViewById(R.id.cio_all_checkins);


        token = FirebaseInstanceId.getInstance().getToken();
        thisPersonRef = personsRef.child(token);


        thisPersonRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot ds) {
                thisPerson = ds.getValue(Person.class);

                if (thisPerson.getCheckins() != null) {
                    checkins = thisPerson.getCheckins();
                    convertCheckinsToStrings(checkins);
                    Collections.reverse(myCheckins);
                } else {
                    myCheckins.add("You have no check-ins yet. Start checking-in to maintain a history.");
                }


                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MyCheckins.this, android.R.layout.simple_list_item_1, myCheckins);

                lv_mycheckins.setAdapter(arrayAdapter);
                lv_mycheckins.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        thisPersonRef.child("posInFB").setValue(myCheckins.size() - position - 1);
//                        cs.posInFb = myCheckins.size() - position - 1;

                        Intent intent = new Intent(getBaseContext(), MyCheckinDetails.class);
                        startActivity(intent);


                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    public void convertCheckinsToStrings(ArrayList<CheckinDetails> checkins) {
        for (CheckinDetails c : checkins) {
            String s = c.getLocationName() + " - " + new SimpleDateFormat("yyyy-MM-dd HH:mm").format(c.getTime());
            myCheckins.add(s);
        }

    }
}
