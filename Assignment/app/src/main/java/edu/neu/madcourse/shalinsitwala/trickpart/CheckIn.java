package edu.neu.madcourse.shalinsitwala.trickpart;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import edu.neu.madcourse.shalinsitwala.R;

public class CheckIn extends AppCompatActivity {

    ClassicSingleton cs = ClassicSingleton.getInstance();

    private static final int PLACE_PICKER_REQUEST = 1;
    LatLngBounds BOUNDS_NEU = new LatLngBounds(
            new LatLng(cs.lat, cs.lon), new LatLng(cs.lat + 1, cs.lon - 1));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trick_activity_check_in);
    }
}
