package edu.neu.madcourse.shalinsitwala.checkitout;

import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

import edu.neu.madcourse.shalinsitwala.R;

public class Answer extends AppCompatActivity {


    ClassicSingleton cs = ClassicSingleton.getInstance();

    DatabaseReference mRoofRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference personsRef = mRoofRef.child("persons");
    DatabaseReference thisPersonRef;
    DatabaseReference placesRef = mRoofRef.child("places");
    DatabaseReference askerRef;
    String token = "";
    Person thisPerson;
    Person asker;

    String asker_email = "";
    String location_name = "";
    CheckinDetails askersCheckin;
    int askerCheckinNumber;
    ArrayList<String> fAnswers = new ArrayList<>();

    TextView tv_person_location_name;
    TextView tv_question;
    ImageButton btn_next;
    Button btn_skip;
    EditText et_answer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cio_activity_answer);

// hiding bar
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.hide();
        }


        tv_person_location_name = (TextView) findViewById(R.id.cio_answer_page_who_and_where);
        tv_question = (TextView) findViewById(R.id.cio_answer_question);
        btn_next = (ImageButton) findViewById(R.id.next_button);
        btn_skip = (Button) findViewById(R.id.skip_button);
        et_answer = (EditText) findViewById(R.id.cio_answer_answer);

        token = FirebaseInstanceId.getInstance().getToken();
        thisPersonRef = personsRef.child(token);


        thisPersonRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot ds) {
                thisPerson = ds.getValue(Person.class);
                askerRef = personsRef.child(thisPerson.getAskerToken());
                askerCheckinNumber = thisPerson.getAskerCheckin();

                askerRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        asker = dataSnapshot.getValue(Person.class);
                        asker_email = asker.getEmail();
                        if (asker_email == null) {
                            asker_email = "Anonymous user";
                        }
                        askersCheckin = asker.getCheckins().get(askerCheckinNumber);
                        location_name = askersCheckin.getLocationName();
                        tv_person_location_name.setText(asker_email + " is at " + location_name);
                        tv_question.setText(askersCheckin.getQuestion());


                        btn_next.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // Push the answer to Firebase
                                if (et_answer.getText().toString() != null) {
                                    String typedAnswer = et_answer.getText().toString();

                                    if (asker.getCheckins().get(askerCheckinNumber).getFanswers() != null) {
                                        // some other friend has already answered
                                        fAnswers = asker.getCheckins().get(askerCheckinNumber).getFanswers();
                                    } else {
                                        // youre the 1st friend to answer
                                        fAnswers = new ArrayList<String>();
                                    }
                                    fAnswers.add(thisPerson.getEmail() + ": " + typedAnswer);
                                    askerRef.child("checkins").child(String.valueOf(askerCheckinNumber)).child("fanswers").setValue(fAnswers);

                                    // So that when asker opens the mycheckinpage via notifications, gets the
                                    // proper checkindetails
                                    askerRef.child("posInFB").setValue(askerCheckinNumber);
                                    // SEND NOTIFICATION TO FRIEND
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            pushNotification(thisPerson.getEmail() + " answered your question.", asker.getToken());
                                        }
                                    }).start();


                                    //Add 10 points
                                    int points = thisPerson.getPoints();
                                    points = points + 10;
                                    thisPersonRef.child("points").setValue(points);
                                    // add points end

                                    Intent intent = new Intent(getBaseContext(), AnswerComplete.class);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    // Nothing typed
                                    Toast.makeText(Answer.this, "You can skip to answer this question", Toast.LENGTH_SHORT).show();
                                }


                            }
                        });


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        btn_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), CheckIn.class);
                startActivity(intent);
                finish();
            }
        });


    }

    private void pushNotification(String body, String to) {
        JSONObject jPayload = new JSONObject();
        JSONObject jNotification = new JSONObject();
        JSONObject jData = new JSONObject();
        try {
            // this content is when app is in background
            jNotification.put("title", "Check It Out");
            jNotification.put("body", body);
            jNotification.put("sound", "default");
            jNotification.put("badge", "1");
            jNotification.put("click_action", "MYCHECKINDETAILSPAGE");

            // If sending to a single client
            jPayload.put("to", to);
            jPayload.put("priority", "high");
            jPayload.put("notification", jNotification);
            jPayload.put("data", jData);

            URL url = new URL("https://fcm.googleapis.com/fcm/send");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", cs.SERVER_KEY);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);

            // Send FCM message content.
            OutputStream outputStream = conn.getOutputStream();
            outputStream.write(jPayload.toString().getBytes());
            outputStream.close();

            // Read FCM response.
            InputStream inputStream = conn.getInputStream();
            final String resp = convertStreamToString(inputStream);

            Handler h = new Handler(Looper.getMainLooper());
            h.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getBaseContext(), resp, Toast.LENGTH_LONG);
                }
            });
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    private String convertStreamToString(InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next().replace(",", ",\n") : "";
    }
}
