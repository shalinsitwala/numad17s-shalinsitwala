package edu.neu.madcourse.shalinsitwala.twoplayergame;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import java.util.Vector;

import edu.neu.madcourse.shalinsitwala.R;


public class ListOfPlayers extends AppCompatActivity {
    private BroadcastReceiver broadcastReceiver;
    Boolean hasInternet;


    LocationManager locationManager;
    LocationListener listener;

    ClassicSingleton cs = ClassicSingleton.getInstance();

    Button btnRegister;
    EditText etEmail;
    String token;
    ArrayList<String> emails = new ArrayList<>();
    String acceptor;
    String gameKey;

    ListView lvEmails;
    DatabaseReference mDataBaseRef;
    Vector<User> availUsers = new Vector<User>();
    TextView tapOnPlayerText;
    Button btnPlaySinglePlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.twop_activity_list_of_players);


        setTitle("Two Player Word Game");

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        checkInternetConnectivity();
        hasInternet = haveNetworkConnection();

        mDataBaseRef = FirebaseDatabase.getInstance().getReference();
        btnRegister = (Button) findViewById(R.id.btnRegister);
        lvEmails = (ListView) findViewById(R.id.lvEmails);
        token = FirebaseInstanceId.getInstance().getToken();
        emails.clear();

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        AlertDialog dialog;

        View mView = getLayoutInflater().inflate(R.layout.dialog_register, null);
        mBuilder.setView(mView);
        dialog = mBuilder.create();
        dialog.setCanceledOnTouchOutside(false);

        final Button btnRegister = (Button) mView.findViewById(R.id.btnRegister);
        etEmail = (EditText) mView.findViewById(R.id.etEmail);

        listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                String add;
                Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                try {
                    List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                    if (addresses.size() > 0) {
                        Address obj = addresses.get(0);

                        add = obj.getCountryName();
                        cs.countryName = add;
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {
            }

            @Override
            public void onProviderEnabled(String s) {
            }

            @Override
            public void onProviderDisabled(String s) {
                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(i);
            }
        };
        configure_button();


        final AlertDialog finalDialog = dialog;
        mDataBaseRef.child("users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(token)) {
                    System.out.println("User already there in database.");
                } else {
                    // pop up the dialog only if this token is not there in the database.

                    finalDialog.show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etEmail.getText().toString().isEmpty()) {
                    //user object
                    User user = new User(etEmail.getText().toString(), token, "", cs.countryName);

                    //add to database
                    DatabaseReference usersRef = mDataBaseRef.child("users");
                    //token is the key here
                    usersRef.child(token).setValue(user);
                    etEmail.setText("");
                    finalDialog.dismiss();
                    finalDialog.dismiss();
                }
            }
        });


        emails.clear();

        // get data for the listview
        this.retrieveData();


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 10:
                configure_button();
                break;
            default:
                break;
        }
    }


    void configure_button() {
        // first check for permissions
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET}
                        , 10);
            }
            return;
        }
        // this code won't execute IF permissions are not allowed, because in the line above there is return statement.
        locationManager.requestLocationUpdates("gps", 5000, 0, listener);
    }

    //Retrieve
    public void retrieveData() {

        mDataBaseRef.child("users").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                getUpdatedList(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void getUpdatedList(DataSnapshot ds) {
        emails.clear();
        for (DataSnapshot data : ds.getChildren()) {
            User us = data.getValue(User.class);
            cs.dbUsers.add(us);

            // to add to the game structure
            if (us.getToken().equalsIgnoreCase(token)) {
                cs.rEmail = us.getEmail();
            }

        }

        for (User u : cs.dbUsers) {
            if (!u.getToken().equalsIgnoreCase(token)) {
                //if token does not exist in the db
                //add it to ListView to show
                if ((!emails.contains(u.getEmail())) && (!emails.contains(u.getEmail() + " : " + u.getCountry()))) {
                    availUsers.add(u);
                    if (cs.countryName != "" || cs.countryName != null) {
                        emails.add(u.getEmail() + " : " + u.getCountry());
                    } else {
                        emails.add(u.getEmail());
                    }

                }

            }
        }

        // availusers is a vector having the user object except for the current user.

        if (emails.size() > 0) {
            ArrayAdapter adapter = new ArrayAdapter(ListOfPlayers.this, android.R.layout.simple_list_item_1, emails);
            lvEmails.setAdapter(adapter);

            // making it clickable here
            lvEmails.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                    Toast.makeText(getBaseContext(),availUsers.elementAt(position).getEmail() + " is selected",Toast.LENGTH_SHORT).show();
                    //Toast.makeText(getBaseContext(), "My token is: " + token + " Opponent's token is: " + availUsers.elementAt(position).getToken(), Toast.LENGTH_SHORT).show();

                    cs.myToken = token;
                    cs.oppToken = availUsers.elementAt(position).getToken();
                    cs.acceptor = availUsers.elementAt(position).getToken();
                    acceptor = availUsers.elementAt(position).getToken();
                    cs.requestor = token;

                    //Getting emails for High Score, Adding to Game structure
                    cs.aEmail = availUsers.elementAt(position).getEmail();


                    //register a game between these 2 players and add to DB
                    int lastClickPosition[] = {-1, -1, -1, -1, -1, -1, -1, -1, -1};
                    ArrayList<Integer> lcp = new ArrayList<Integer>();
                    for (int index = 0; index < lastClickPosition.length; index++) {
                        lcp.add(lastClickPosition[index]);
                    }

                    String selectedLetters[] = {"", "", "", "", "", "", "", "", ""};
                    ArrayList<String> sl = new ArrayList<String>(Arrays.asList(selectedLetters));

                    String trueWords[] = {"", "", "", "", "", "", "", "", ""};
                    ArrayList<String> tw = new ArrayList<String>(Arrays.asList(trueWords));
                    Game game = new Game(token, availUsers.elementAt(position).getToken(), 0, 0, new ArrayList<String>(), new ArrayList<String>(),
                            new Date(), "P1", lcp, sl, tw, cs.rEmail, cs.aEmail);

                    DatabaseReference gamesRef = mDataBaseRef.child("games");
//                    gamesRef.push().setValue(game);

                    DatabaseReference postRef = gamesRef.push();
                    postRef.setValue(game);

                    gameKey = postRef.getKey();

                    // Add this gamekey to acceptor's DB
                    DatabaseReference usersRef = mDataBaseRef.child("users");
                    usersRef.child(acceptor).child("currentGame").setValue(gameKey);
                    usersRef.child(token).child("currentGame").setValue(gameKey);

                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                    SharedPreferences.Editor editor = preferences.edit();
                    cs.gameKey = gameKey;
                    editor.putString("gameKey", gameKey);
                    editor.putString("requestor", token);
                    editor.apply();
                    // SEND NOTIFICATION TO OPPONENT!
                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            pushNotification("Someone challenged you!");
                        }
                    }).start();


                    Intent intent = new Intent(ListOfPlayers.this, GameActivity.class);
//                    intent.putExtra("requestor", token);

//                    intent.putExtra("oppToken", availUsers.elementAt(position).getToken());
                    startActivity(intent);
                }
            });


        } else {
            Toast.makeText(ListOfPlayers.this, "No other users. Please try later.", Toast.LENGTH_SHORT).show();
        }
    }

    private void pushNotification(String body) {
        JSONObject jPayload = new JSONObject();
        JSONObject jNotification = new JSONObject();
        JSONObject jData = new JSONObject();
        try {
            // this content is when app is in background
            jNotification.put("title", "Scroggle!");
            jNotification.put("body", body);
            jNotification.put("sound", "default");
            jNotification.put("badge", "1");
            jNotification.put("click_action", "TWOPGAME");

//            jData.put("requestor", token);
//            jData.put("acceptor", acceptor);
            jData.put("gameKey", gameKey);

            // If sending to a single client
            jPayload.put("to", cs.oppToken);

            /*
            // If sending to multiple clients (must be more than 1 and less than 1000)
            JSONArray ja = new JSONArray();
            ja.put(CLIENT_REGISTRATION_TOKEN);
            // Add Other client tokens
            ja.put(FirebaseInstanceId.getInstance().getToken());
            jPayload.put("registration_ids", ja);
            */

            jPayload.put("priority", "high");
            jPayload.put("notification", jNotification);
            jPayload.put("data", jData);

            URL url = new URL("https://fcm.googleapis.com/fcm/send");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", cs.SERVER_KEY);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);

            // Send FCM message content.
            OutputStream outputStream = conn.getOutputStream();
            outputStream.write(jPayload.toString().getBytes());
            outputStream.close();

            // Read FCM response.
            InputStream inputStream = conn.getInputStream();
            final String resp = convertStreamToString(inputStream);

            Handler h = new Handler(Looper.getMainLooper());
            h.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getBaseContext(), resp, Toast.LENGTH_LONG);
                }
            });
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    private String convertStreamToString(InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next().replace(",", ",\n") : "";
    }

    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    //checks continuously
    private void checkInternetConnectivity() {
        IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int[] type = {ConnectivityManager.TYPE_WIFI, ConnectivityManager.TYPE_MOBILE};
                if (CheckInternetBroadcast.isNetworkAvailable(context, type)) {
                    setupLayoutInternetFound();
                } else {
                    Toast.makeText(ListOfPlayers.this, "Please connect to the internet", Toast.LENGTH_SHORT).show();
                    hideStuffWhenInternetNotFound();
                }
            }
        };

        //register receiver
        registerReceiver(broadcastReceiver, intentFilter);
    }


    protected void setupLayoutInternetFound() {
        tapOnPlayerText = (TextView) findViewById(R.id.tapOnPlayerText);
        tapOnPlayerText.setVisibility(View.VISIBLE);
        btnPlaySinglePlayer = (Button) findViewById(R.id.btnPlaySinglePlayer);
        btnPlaySinglePlayer.setVisibility(View.GONE);

        emails.clear();

        // get data for the listview
        this.retrieveData();
    }

    protected void hideStuffWhenInternetNotFound() {
        emails.clear();
        emails.add("Please try again with an active Internet connection.");
        emails.add("Meanwhile, you can play the single player game!");
        ArrayAdapter adapter = new ArrayAdapter(ListOfPlayers.this, android.R.layout.simple_list_item_1, emails);
        lvEmails.setAdapter(adapter);

        tapOnPlayerText = (TextView) findViewById(R.id.tapOnPlayerText);
        tapOnPlayerText.setVisibility(View.INVISIBLE);

        btnPlaySinglePlayer = (Button) findViewById(R.id.btnPlaySinglePlayer);
        btnPlaySinglePlayer.setVisibility(View.VISIBLE);

        btnPlaySinglePlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListOfPlayers.this, edu.neu.madcourse.shalinsitwala.wordgame.MainActivity.class);
                startActivity(intent);
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }


}
