package edu.neu.madcourse.shalinsitwala.checkitout;

import java.util.ArrayList;

public class Person {

    String token;
    String email;
    ArrayList<Friend> friends;
    ArrayList<CheckinDetails> checkins;
    int points;
    String askerToken;
    int askerCheckin;
    int posInFB;

    public int getPosInFB() {
        return posInFB;
    }

    public void setPosInFB(int posInFB) {
        this.posInFB = posInFB;
    }

    public String getAskerToken() {
        return askerToken;
    }

    public void setAskerToken(String askerToken) {
        this.askerToken = askerToken;
    }

    public int getAskerCheckin() {
        return askerCheckin;
    }

    public void setAskerCheckin(int askerCheckin) {
        this.askerCheckin = askerCheckin;
    }

    public Person(String token) {
        this.token = token;
    }

    public Person() {

    }

    public String getToken() {

        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ArrayList<Friend> getFriends() {
        return friends;
    }

    public void setFriends(ArrayList<Friend> friends) {
        this.friends = friends;
    }

    public ArrayList<CheckinDetails> getCheckins() {
        return checkins;
    }

    public void setCheckins(ArrayList<CheckinDetails> checkins) {
        this.checkins = checkins;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
