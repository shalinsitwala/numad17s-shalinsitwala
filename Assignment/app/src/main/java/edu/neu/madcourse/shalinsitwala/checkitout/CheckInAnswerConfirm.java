package edu.neu.madcourse.shalinsitwala.checkitout;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import edu.neu.madcourse.shalinsitwala.R;

public class CheckInAnswerConfirm extends AppCompatActivity {


    ImageButton btn_next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cio_activity_check_in_answer_confirm);

        // hiding bar
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.hide();
        }

        btn_next = (ImageButton) findViewById(R.id.next_button);


        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), CheckIn.class);
                startActivity(intent);
            }
        });
    }
}
