package edu.neu.madcourse.shalinsitwala.twoplayergame;

/**
 * Created by shali on 27-03-2017.
 */

public class MyTile {

    private String text;
    private String status;


    public MyTile() {
        this.text = "";
        this.status = "Blank";
    }

    public MyTile(String text, String status) {
        this.text = text;
        this.status = status;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
