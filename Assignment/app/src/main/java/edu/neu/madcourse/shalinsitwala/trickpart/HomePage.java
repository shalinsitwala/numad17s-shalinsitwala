package edu.neu.madcourse.shalinsitwala.trickpart;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import edu.neu.madcourse.shalinsitwala.R;

public class HomePage extends AppCompatActivity {
    ClassicSingleton cs = ClassicSingleton.getInstance();


    final int PLACE_PICKER_REQUEST = 1;
    //    LatLngBounds BOUNDS_NEU = new LatLngBounds(new LatLng(42.3398106, -71.0913604), new LatLng(42.3398106+0.003245, -71.0913604+0.00208741));
    LatLngBounds BOUNDS_NEU = new LatLngBounds(new LatLng(cs.lat, cs.lon), new LatLng(cs.lat + 0.003245, cs.lon + 0.00208741));


    TextView yourLatLong;
    Button getLatLongBtn;
    TextView checkedinLocation;
    Button checkInBtn;
    Button btnAssig11Ack;
    Geocoder geocoder;
    List<Address> addresses;
    LocationListener locationListener;
    LocationManager locationManager;
    LatLng checkedInLatLng;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trick_activity_home_page);

        setTitle("Trickiest Part");

        yourLatLong = (TextView) findViewById(R.id.textView12);
        getLatLongBtn = (Button) findViewById(R.id.button2);
        checkedinLocation = (TextView) findViewById(R.id.textView14);
        checkInBtn = (Button) findViewById(R.id.checkinbtn);
        btnAssig11Ack = (Button) findViewById(R.id.btnAssig11Ack);
        geocoder = new Geocoder(this, Locale.getDefault());
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);


        getLatLong();
        Spanned text = Html.fromHtml("Your Latitude: <b>" + cs.lat + "</b> <BR>Longitude: <b>" + cs.lon + "</b>");
        yourLatLong.setText(text);


        btnAssig11Ack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog mDialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(HomePage.this);
                builder.setTitle("Assignment 11 Acknowledgements");
                builder.setMessage(Html.fromHtml(getString(R.string.ack11_text)));
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.ok_label,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface,
                                                int i) {
                                // nothing
                            }
                        });
                mDialog = builder.show();
            }
        });

        checkInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();
                    intentBuilder.setLatLngBounds(BOUNDS_NEU);
                    Intent intent = intentBuilder.build(HomePage.this);
                    startActivityForResult(intent, PLACE_PICKER_REQUEST);

                } catch (GooglePlayServicesRepairableException
                        | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PLACE_PICKER_REQUEST && resultCode == Activity.RESULT_OK) {

            final Place place = PlacePicker.getPlace(this, data);
            final CharSequence name = place.getName();
            final CharSequence address = place.getAddress();


            String attributions = (String) place.getAttributions();
            if (attributions == null) {
                attributions = "";
            }

            checkedInLatLng = place.getLatLng();
            float[] results = new float[1];
            Location.distanceBetween(cs.lat, cs.lon, checkedInLatLng.latitude, checkedInLatLng.longitude, results);
            float distance = results[0];

            distance = distance * (float) 0.00062137;

            Spanned checkindetails = Html.fromHtml("<i>Checked-in at: </i><BR><b>Name:</b> " + name + " <BR><b>Address:</b> "
                    + address + "<BR><b>Distance from your location:</b> " + String.valueOf(distance) + "miles");
            checkedinLocation.setText(checkindetails);


//            mName.setText(name);
//            mAddress.setText(address);
//            mAttributions.setText(Html.fromHtml(attributions));

        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    public void getLatLong() {
// Define a listener that responds to location updates
        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.

                makeUseOfNewLocation(location);
//                Toast.makeText(getBaseContext(), String.valueOf(location.getLatitude()), Toast.LENGTH_LONG).show();
                cs.lat = location.getLatitude();
                cs.lon = location.getLongitude();

            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
//                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                startActivity(i);
            }
        };

        configure_button();

        // Register the listener with the Location Manager to receive location updates
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 10:
                configure_button();
                break;
            default:
                break;
        }
    }

    void configure_button() {
        // first check for permissions
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET}
                        , 10);
            }
            return;
        }
        // this code won't execute IF permissions are not allowed, because in the line above there is return statement.
        locationManager.requestLocationUpdates("gps", 5000, 0, locationListener);
    }

    public void makeUseOfNewLocation(final Location location) {
        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 5);
//            System.out.println("Blah" + addresses.size());
//            System.out.println(addresses.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }

        getLatLongBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getBaseContext(), String.valueOf(location.getLatitude()) + "  " + String.valueOf(location.getLongitude()), Toast.LENGTH_SHORT).show();
                cs.lat = location.getLatitude();
                cs.lon = location.getLongitude();
                Spanned text = Html.fromHtml("Your Latitude: <b>" + cs.lat + "</b> <BR>Longitude: <b>" + cs.lon + "</b>");
                yourLatLong.setText(text);
            }
        });

    }
}
