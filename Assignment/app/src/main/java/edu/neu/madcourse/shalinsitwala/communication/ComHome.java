package edu.neu.madcourse.shalinsitwala.communication;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.params.CoreConnectionPNames;

import java.util.concurrent.BrokenBarrierException;

import edu.neu.madcourse.shalinsitwala.R;

public class ComHome extends AppCompatActivity {

    private BroadcastReceiver broadcastReceiver;
    ClassicSingleton cs = ClassicSingleton.getInstance();

    Button btnGoToPlayerList, btnAssig7Ack, btnHighScores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.com_activity_home);
        setTitle("Communication");
        checkInternetConnectivity();

        btnAssig7Ack = (Button) findViewById(R.id.btnAssig7Ack);
        btnGoToPlayerList = (Button) findViewById(R.id.btnGoToPlayerList);
        btnHighScores = (Button) findViewById(R.id.btnHighScores);


        btnGoToPlayerList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ComHome.this, ListOfPlayers.class);
                startActivity(intent);
            }
        });

        btnAssig7Ack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog mDialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(ComHome.this);
                builder.setTitle("Assignment 7 Acknowledgements");
                builder.setMessage(Html.fromHtml(getString(R.string.ack7_text)));
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.ok_label,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface,
                                                int i) {
                                // nothing
                            }
                        });
                mDialog = builder.show();
            }
        });

        btnHighScores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), HighScores.class);
                startActivity(intent);
            }
        });


    }


    private void checkInternetConnectivity() {
        IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int[] type = {ConnectivityManager.TYPE_WIFI, ConnectivityManager.TYPE_MOBILE};
                if (CheckInternetBroadcast.isNetworkAvailable(context, type)) {
                    return;
                } else {
                    Toast.makeText(ComHome.this, "Please connect to the internet", Toast.LENGTH_SHORT).show();
                }
            }
        };

        //register receiver
        registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

}
