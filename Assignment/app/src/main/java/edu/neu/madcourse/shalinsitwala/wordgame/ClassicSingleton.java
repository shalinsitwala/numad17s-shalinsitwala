package edu.neu.madcourse.shalinsitwala.wordgame;

import android.media.MediaPlayer;

import java.util.Vector;

import edu.neu.madcourse.shalinsitwala.R;

public class ClassicSingleton {



    int points=0;

    // Timer text
    long timerVal;

    int phase;


    int score = 0;

//    boolean playMusic = true;
//    MediaPlayer mMediaPlayer;

    //for the dict in game
    public Vector<String> loaded = new Vector<String>();

    String check;


    //String nineWords[] = {"COMPUTERS", "BLIZZARDS", "STREAKING", "SQUEEZING"};
    String nineWords[] = {"GABERONES","GABIONADE","GABIONAGE","GABLELIKE","GABLEWISE","GABNASHES","GADABOUTS","GADARENES","GADDINGLY","GADGETEER","GADOLINIA","GADOLINIC","GADROONED","ACROMETER","ACROMIMIA","ACROMIONS","ACRONICAL","ACRONYCAL","ACRONYMIC","ACROPATHY","ACROPETAL","ACROPHOBE","ACROPHONY","ACROPODIA","ACROPOLIS","ACROSARCA","ACROSOMAL","ACROSOMES","ACROSPIRE","ACROSPORE","ACROSTICS","ACROTERAL","ACROTERIA","ACROTERIC","ACROTISMS","ACRYLATES","ACTINALLY","ACTINARIA","ACTINIANS","ACTINICAL","ACTINIDES","ACTINIDIA","ACTINISMS","ACTINIUMS","ACTINOIDS","ACTINOPOD","ACTINOZOA","BACCHEION","BACCIFORM","BACCILLLA","BACCILLUM","BACHARACH","BACHELORS","BACILLARY","BACILLIAN","BACILLITE","BACKACHES","BACKARROW","BACKBANDS","CACODEMON","CACODYLIC","CACOEPIES","CACOEPIST","CACOETHES","CACOETHIC","CACOGENIC","DACRYDIUM","DACRYOLIN","DACRYURIA","DACTYLATE","DACTYLICS","DACTYLION","EASTABOUT","EASTBOUND","EASTERING","EASTERNER","EASTERNLY","EASTLANDS","ICEFIELDS","ICEFISHES","ICEHOUSES","ICEKHANAS","ICELANDER","ICELANDIC","ICEMAKERS","ICESCAPES","ICESKATED","ICESTONES","LACKLANDS","LACKSENSE","LACONIANS","LACONICAL","LACONICUM","LACONISMS","MARGARITE","MARGELINE","MARGENTED","MARGINALS","MARGINATE","MARGINING","MARGRAVES","MARGULLIE","MARIACHIS","MARIALITE","OBSCENELY","OBSCENEST","OBSCENITY","OBSCURANT","OBSCURELY","OBSCURERS","OBSCUREST","OBSCURING","OBSCURISM","OBSCURIST","OBSCURITY","OBSECRATE","OBSEQUENT","RADIOLEAD","RADIOLITE","RADIOLOGY","RADIONICS","RADIORAYS","RADIOTHON","RADIUMIZE","RADKNIGHT","SHITTIEST","SHIVAISMS","SHIVAISTS","SHIVAREED","SHIVAREES","SHIVERERS","SHIVERIER","SHIVERING","TAMBOURIN","TAMBURINS","TAMBURONE","TAMERLANE","TAMOXIFEN","TAMPERERS","TAMPERING","TAMPIONED","TAMPONADE","UNACUTELY","UNADAMANT","UNADAPTED","UNADDABLE","UNADDIBLE","UNADDRESS","VANCOUVER","VANDALISH","VANDALISM","VANDALIZE","VANDYKING","WOODHOLES","WOODHORSE","WOODHOUSE","WOODINESS","WOODLANDS","WOODLARKS","WOODLORES","WOODLOUSE","WOODMEALS","WOODMOUSE","WOODNOTES","XYLOCAINE","XYLOCARPS","XYLOCOPID","XYLOGRAPH","XYLOIDINE","XYLOIDINS","XYLOMANCY","XYLOMELUM","XYLOMETER","XYLONITES","XYLOPHAGE","XYLOPHONE","ZEALOTIST","ZEALOUSLY","ZEALPROOF","ZEBRAFISH","ZEBRALIKE","ZEBRASSES","ZEBRAWOOD","ZECCHINES","ZECCHINOS","ZECHARIAH","YOUTHTIDE","YOUTHWORT","YPSILANTI","YRAVISHED","YSHENDING","YTTERBIAS","YTTERBITE","YTTERBIUM","YTTERBOUS","YUCATECAN","YUCKINESS","YUGOSLAVS","YUKAGHIRS","YULEBLOCK"};

    // blizzards, computers, streaking, squeeze, postualte, spectator, chemistry
    int allPatterns[][] = {{1, 2, 8, 0, 3, 7, 4, 5, 6}, {8, 7, 6, 3, 4, 5, 2, 1, 0}, {3, 4, 5, 2, 1, 6, 0, 8, 7}, {2, 1, 8, 3, 0, 7, 4, 5, 6}, {8, 1, 2, 7, 0, 3, 6, 5, 4}};

    int lastClickPosition[] = {-1, -1, -1, -1, -1, -1, -1, -1, -1};


    String selectedLetters[] = {"", "", "", "", "", "", "", "", ""};
    int[][] selectedPositions = new int[9][9];
    Tile mLargeTiles[] = new Tile[9];
    Tile mSmallTiles[][] = new Tile[9][9];
    Tile mEntireBoard = null;

    public int mLargeIds[] = {R.id.large1, R.id.large2, R.id.large3,
            R.id.large4, R.id.large5, R.id.large6, R.id.large7, R.id.large8,
            R.id.large9,};
    public int mSmallIds[] = {R.id.small1, R.id.small2, R.id.small3,
            R.id.small4, R.id.small5, R.id.small6, R.id.small7, R.id.small8,
            R.id.small9,};




    String trueWords[] = {"","","","","","","","",""};

    Tile p2board = null;
    Tile p2tiles[] = new Tile[9];
    int p2ids[] = {R.id.small1, R.id.small2, R.id.small3,
            R.id.small4, R.id.small5, R.id.small6, R.id.small7, R.id.small8,
            R.id.small9,};

    int p2lastclick = -1;
    String p2selectedletters = "";
    String p2randomword = "";

    String vowels = "AEIOU";

    ///////////////////////////////////////////////////////////////////////////////////////////


    private static ClassicSingleton instance = null;

    private ClassicSingleton() {
        // Exists only to defeat instantiation.
    }

    public static ClassicSingleton getInstance() {
        if (instance == null) {
            instance = new ClassicSingleton();
        }
        return instance;
    }

    public static void ResetSingleton() {
        instance = null;
    }
}