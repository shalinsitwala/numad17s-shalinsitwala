package edu.neu.madcourse.shalinsitwala.checkitout;


import java.util.ArrayList;

public class SetOfQnA {

    ArrayList<QnA> setOfQA;

    public SetOfQnA(ArrayList<QnA> setOfQA) {
        this.setOfQA = setOfQA;
    }

    public ArrayList<QnA> getSetOfQA() {

        return setOfQA;
    }

    public void setSetOfQA(ArrayList<QnA> setOfQA) {
        this.setOfQA = setOfQA;
    }
}
