package edu.neu.madcourse.shalinsitwala;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

public class showAck extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_ack);
        setTitle("Acknowledgements");

        TextView tv = (TextView) findViewById(R.id.assignment3ack);
        tv.setText(Html.fromHtml(getString(R.string.assignment3_ack)));


    }

    public void goBack(View v){
        finish();
    }
}
