package edu.neu.madcourse.shalinsitwala;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.TextView;

public class DisplayAboutMeDetails extends AppCompatActivity {


    TelephonyManager tel;
    TextView imei;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_about_me_details);

        setTitle("Shalin Sitwala");

        tel = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        imei = (TextView) findViewById(R.id.imeiNumber);
        imei.setText(tel.getDeviceId().toString());
    }


}
