package edu.neu.madcourse.shalinsitwala.communication;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by shali on 04-03-2017.
 */

public class Game {
    private String requestor;
    private String acceptor;
    private int rscore;
    private int ascore;
    private ArrayList<String> words;

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    private Date time;

    public Game(String requestor, String acceptor, int rscore, int ascore, ArrayList<String> words, Date time) {
        this.requestor = requestor;
        this.acceptor = acceptor;
        this.rscore = rscore;
        this.ascore = ascore;
        this.words = words;
        this.time = time;
    }

    public ArrayList<String> getWords() {
        return words;
    }

    public void setWords(ArrayList<String> words) {
        this.words = words;
    }


    public int getRscore() {
        return rscore;
    }

    public void setRscore(int rscore) {
        this.rscore = rscore;
    }

    public int getAscore() {
        return ascore;
    }

    public void setAscore(int ascore) {
        this.ascore = ascore;
    }


    public Game() {
        this.acceptor = "";
        this.requestor = "";
        this.rscore = 0;
        this.ascore = 0;
        this.words = new ArrayList<String>();
        this.time = new Date();
    }

    public String getAcceptor() {
        return acceptor;
    }

    public void setAcceptor(String acceptor) {
        this.acceptor = acceptor;
    }

    public String getRequestor() {
        return requestor;
    }

    public void setRequestor(String requestor) {
        this.requestor = requestor;
    }
}
