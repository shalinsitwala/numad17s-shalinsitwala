package edu.neu.madcourse.shalinsitwala.wordgame;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import edu.neu.madcourse.shalinsitwala.R;


public class GameOver extends Fragment {
    public GameOver() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View rootView =
                inflater.inflate(R.layout.wgfragment_game_over, container, false);
        initViews(rootView);

        return rootView;

    }

    public void initViews(View v){
        ClassicSingleton cs = ClassicSingleton.getInstance();
        TextView finalScoreText = (TextView)v.findViewById(R.id.finalScore);
        finalScoreText.setTextColor(Color.WHITE);
        finalScoreText.setText("Final score is: " + cs.score);
    }


}
