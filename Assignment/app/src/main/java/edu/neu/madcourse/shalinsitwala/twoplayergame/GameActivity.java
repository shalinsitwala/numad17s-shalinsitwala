package edu.neu.madcourse.shalinsitwala.twoplayergame;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import org.w3c.dom.Text;

import edu.neu.madcourse.shalinsitwala.R;


import edu.neu.madcourse.shalinsitwala.twoplayergame.GameOver;

public class GameActivity extends AppCompatActivity {
    ClassicSingleton cs = ClassicSingleton.getInstance();

    DatabaseReference mRoofRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference usersRef = mRoofRef.child("users");
    String requestor = "";
    String acceptor = "";


    public CountDownTimer countDownTimer;
    GameFragment gf = new GameFragment();


    //    GameFragment2 gf2 = new GameFragment2();
    GameOver go = new GameOver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.twop_activity_game);

        // hiding bar
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.hide();
        }

        Intent startingIntent = getIntent();
        requestor = startingIntent.getStringExtra("requestor");
        acceptor = startingIntent.getStringExtra("acceptor");

        String source = startingIntent.getStringExtra("source");
        if (source != null) {
            if (source.equalsIgnoreCase("ForegroundAcceptor")) {
                //then find the gamekey from the users DB.
                String token = FirebaseInstanceId.getInstance().getToken();

                DatabaseReference thisUserRef = usersRef.child(token);
                thisUserRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot ds) {
                        User u = ds.getValue(User.class);
                        cs.gameKey = u.getCurrentGame();
                        if (cs.gameKey != null) {
                            addFragment();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            }
        }


        if (cs.gameKey == null) {
            //getting this data from notification sent when app in background
            if (getIntent().getExtras() != null) {
                cs.gameKey = getIntent().getExtras().getString("gameKey");
            }
        }

    }


    public void quitGame(View v) {
        ClassicSingleton.ResetSingleton();
        Intent intent = new Intent(this, edu.neu.madcourse.shalinsitwala.MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public String getAcceptor() {
        return acceptor;
    }


    public String getRequestor() {
        return requestor;
    }

    @Override
    protected void onResume() {

        super.onResume();


        if (cs.phase == 0) {
            cs.phase = 1;
        }
//        TextView tv = (TextView) findViewById(R.id.score);
//        tv.setText(String.valueOf(cs.score));

        TextView tvp1score = (TextView) findViewById(R.id.p1score);
        TextView tvp2score = (TextView) findViewById(R.id.p2score);

        tvp1score.setText("P1: " + String.valueOf(cs.rscore) + "  ");
        tvp2score.setText("P2: " + String.valueOf(cs.ascore));


        if (cs.gameKey != null) {
            addFragment();
        }


        countDownTimer = new GameTimer(cs.timerVal, 1000); // made the timer here, rest is done in the timer class.
        countDownTimer.start();
    }

    @Override
    protected void onPause() {
        countDownTimer.cancel();
        super.onPause();
    }


    public void addFragment() {

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        if (cs.phase == 1) {
            transaction.replace(R.id.fragment_container, gf);
        }
//        if (cs.phase == 2) {
//            transaction.replace(R.id.fragment_container, gf2);
//        }
        if (cs.phase == 3) {
            transaction.replace(R.id.fragment_container, new GameOver());
            //hide stuff
//            TextView phaseNameText = (TextView) findViewById(R.id.phaseName);
//            TextView scoreText = (TextView) findViewById(R.id.score);
            TextView gameTimerText = (TextView) findViewById(R.id.gameTimer);
//            TextView scoreStaticText = (TextView) findViewById(R.id.scoreStatic);

//            phaseNameText.setVisibility(View.INVISIBLE);
//            Button pauseBtn = (Button) findViewById(R.id.pauseGameBtn);
//            pauseBtn.setVisibility(View.INVISIBLE);
//            scoreText.setVisibility(View.INVISIBLE);
            gameTimerText.setVisibility(View.INVISIBLE);
//            scoreStaticText.setVisibility(View.INVISIBLE);

        }
        transaction.commit();
    }


    public class GameTimer extends CountDownTimer {


        TextView gameTimerText = (TextView) findViewById(R.id.gameTimer);
        TextView scoreStatic = (TextView) findViewById(R.id.scoreStatic);
        //        TextView phaseNameText = (TextView) findViewById(R.id.phaseName);
//        TextView scoreText = (TextView) findViewById(R.id.score);
        TextView tvp1score = (TextView) findViewById(R.id.p1score);
        TextView tvp2score = (TextView) findViewById(R.id.p2score);
        TextView tvTimeStatic = (TextView) findViewById(R.id.timeStatic);


        //constructor
        public GameTimer(long startTime, long interval) {
            super(startTime, interval);
        }

        @Override
        public void onTick(long millisUntilFinished) {

            cs.timerVal = millisUntilFinished;


//            scoreText.setText(String.valueOf(cs.score));
            tvp1score.setText("P1: " + String.valueOf(cs.rscore) + "  ");
            tvp2score.setText("P2: " + String.valueOf(cs.ascore));


            gameTimerText.setText((cs.timerVal / 60000) + ":" + (cs.timerVal % 60000 / 1000));
//            phaseNameText.setText("Phase: " + cs.phase);


            if (millisUntilFinished <= 15 * 1000 && millisUntilFinished >= 12 * 1000 && cs.phase == 1) {
//                Toast.makeText(getApplicationContext(), "Phase 1 ending soon!", Toast.LENGTH_SHORT).show();
                Toast.makeText(getApplicationContext(), "Game ending soon!", Toast.LENGTH_SHORT).show();
            }


            if (millisUntilFinished <= 15 * 1000 && millisUntilFinished >= 12 * 1000 && cs.phase == 2) {
                Toast.makeText(getApplicationContext(), "Game ending soon!",
                        Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        public void onFinish() {

            FragmentManager manager1 = getSupportFragmentManager();
            FragmentTransaction transaction1 = manager1.beginTransaction();
            transaction1.replace(R.id.fragment_container, new GameOver());
            transaction1.commit();
            hideStuffForGameOver();


//            if (cs.phase == 1) { // when phase 1 ends
//                // change to phase 2
//                cs.phase = 2;
////                phaseNameText.setText("Phase: " + cs.phase);
//
//                FragmentManager manager1 = getSupportFragmentManager();
//                FragmentTransaction transaction1 = manager1.beginTransaction();
//                transaction1.replace(R.id.fragment_container, new GameFragment2());
//                transaction1.commit();
//
//                // start new timer here of 90 more seconds
//
//                cs.timerVal = 90 * 1000;
//                countDownTimer = new GameTimer(cs.timerVal, 1000);
//                countDownTimer.start();
//            } else if (cs.phase == 2) {
//                cs.phase = 3;
//                FragmentManager manager1 = getSupportFragmentManager();
//                FragmentTransaction transaction1 = manager1.beginTransaction();
//                transaction1.replace(R.id.fragment_container, go);
//                transaction1.commit();
//                // Hide things from the activity bar
//                hideStuffForGameOver();
//            }


//            gameTimerText.setText("Done");


        }

        public void hideStuffForGameOver() {
//            phaseNameText.setVisibility(View.INVISIBLE);
//            Button pauseBtn = (Button) findViewById(R.id.pauseGameBtn);
//            TextView scoreStaticText = (TextView) findViewById(R.id.scoreStatic);
//            scoreStaticText.setVisibility(View.INVISIBLE);
//            pauseBtn.setVisibility(View.INVISIBLE);
//            scoreText.setVisibility(View.INVISIBLE);


            scoreStatic.setVisibility(View.INVISIBLE);
            tvp1score.setVisibility(View.INVISIBLE);
            tvp2score.setVisibility(View.INVISIBLE);
            tvTimeStatic.setVisibility(View.INVISIBLE);
            gameTimerText.setVisibility(View.INVISIBLE);


        }
    }
}


