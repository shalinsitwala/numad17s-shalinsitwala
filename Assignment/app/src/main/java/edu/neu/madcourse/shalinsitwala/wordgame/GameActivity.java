package edu.neu.madcourse.shalinsitwala.wordgame;


import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.concurrent.TimeUnit;

import edu.neu.madcourse.shalinsitwala.*;


public class GameActivity extends AppCompatActivity {
    public MediaPlayer bgPlayer;

    GameFragment gf = new GameFragment();
    GameFragment2 gf2 = new GameFragment2();
    GameOver go = new GameOver();


    ClassicSingleton cs = ClassicSingleton.getInstance();
    MusicSingleton ms = MusicSingleton.getInstance();


    public CountDownTimer countDownTimer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wgactivity_game);


        // hiding bar
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.hide();
        }

        bgPlayer = MediaPlayer.create(this, R.raw.acousticbreeze);
        bgPlayer.setLooping(true);


    }


    @Override
    protected void onResume() {
        if (ms.playMusic) {
            startMusic();
        } else {
            stopMusic();
        }
        super.onResume();


        if (cs.phase == 0) {
            cs.phase = 1;
        }
        TextView tv = (TextView) findViewById(R.id.score);
        tv.setText(String.valueOf(cs.score));

        addFragment();


        countDownTimer = new GameTimer(cs.timerVal, 1000); // made the timer here, rest is done in the timer class.
        countDownTimer.start();
    }

    @Override
    protected void onPause() {
        countDownTimer.cancel();
        stopMusic();

        super.onPause();


    }

    public void addFragment() {

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        if (cs.phase == 1) {
            transaction.replace(R.id.fragment_container, gf);
        }
        if (cs.phase == 2) {
            transaction.replace(R.id.fragment_container, gf2);
        }
        if (cs.phase == 3) {
            transaction.replace(R.id.fragment_container, new GameOver());
            //hide stuff
            TextView phaseNameText = (TextView) findViewById(R.id.phaseName);
            TextView scoreText = (TextView) findViewById(R.id.score);
            TextView gameTimerText = (TextView) findViewById(R.id.gameTimer);
            TextView scoreStaticText = (TextView) findViewById(R.id.scoreStatic);

            phaseNameText.setVisibility(View.INVISIBLE);
            Button pauseBtn = (Button) findViewById(R.id.pauseGameBtn);
            pauseBtn.setVisibility(View.INVISIBLE);
            scoreText.setVisibility(View.INVISIBLE);
            gameTimerText.setVisibility(View.INVISIBLE);
            scoreStaticText.setVisibility(View.INVISIBLE);

        }


        transaction.commit();


    }

    public void pauseGame(View v) {
        countDownTimer.cancel();
        FragmentManager manager1 = getSupportFragmentManager();
        FragmentTransaction transaction1 = manager1.beginTransaction();
        transaction1.replace(R.id.fragment_container, new ResumeFragment());
        transaction1.commit();
    }

    public void resumeGame(View v) {
        countDownTimer = new GameTimer(cs.timerVal, 1000);
        countDownTimer.start();
        FragmentManager manager1 = getSupportFragmentManager();
        FragmentTransaction transaction1 = manager1.beginTransaction();

        if (cs.phase == 1 || cs.phase == 0) {
            transaction1.replace(R.id.fragment_container, gf);
        } else if (cs.phase == 2) {
            transaction1.replace(R.id.fragment_container, gf2);
            // current word displayed on tap in gf2. so if there's no tap, word wont show up after pause. so adding below.
//            p2CurrentWordText.setText(cs.p2selectedletters);
        } else if (cs.phase == 3) {
            transaction1.replace(R.id.fragment_container, new GameOver());
        }


        transaction1.commit();
    }

    public void quitGame(View v) {
//        finish(); // goes back to main menu of the game


        //below code makes it go to the main menu of the main app
        ClassicSingleton.ResetSingleton();
        Intent intent = new Intent(this, edu.neu.madcourse.shalinsitwala.MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);


    }

    public void startMusic() {

        bgPlayer.start();
    }

    public void stopMusic() {

        if (bgPlayer.isPlaying()) {

            bgPlayer.pause();
        }
    }


    public class GameTimer extends CountDownTimer {


        TextView gameTimerText = (TextView) findViewById(R.id.gameTimer);
        TextView phaseNameText = (TextView) findViewById(R.id.phaseName);
        TextView scoreText = (TextView) findViewById(R.id.score);

        //constructor
        public GameTimer(long startTime, long interval) {
            super(startTime, interval);
        }

        @Override
        public void onTick(long millisUntilFinished) {

            cs.timerVal = millisUntilFinished;


            scoreText.setText(String.valueOf(cs.score));
//            gameTimerText.setText(String.valueOf(cs.timerVal / 1000l));

            gameTimerText.setText((cs.timerVal/60000) + ":" + (cs.timerVal % 60000 / 1000));
            phaseNameText.setText("Phase: " + cs.phase);


            if (millisUntilFinished <= 15 * 1000 && millisUntilFinished >= 12 * 1000 && cs.phase == 1) {
                Toast.makeText(getApplicationContext(), "Phase 1 ending soon!",
                        Toast.LENGTH_SHORT).show();
            }


            if (millisUntilFinished <= 15 * 1000 && millisUntilFinished >= 12 * 1000 && cs.phase == 2) {
                Toast.makeText(getApplicationContext(), "Game ending soon!",
                        Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        public void onFinish() {


            if (cs.phase == 1) { // when phase 1 ends
                // change to phase 2
                cs.phase = 2;
                phaseNameText.setText("Phase: " + cs.phase);

                FragmentManager manager1 = getSupportFragmentManager();
                FragmentTransaction transaction1 = manager1.beginTransaction();
                transaction1.replace(R.id.fragment_container, new GameFragment2());
                transaction1.commit();

                // start new timer here of 90 more seconds

                cs.timerVal = 90 * 1000;
                countDownTimer = new GameTimer(cs.timerVal, 1000);
                countDownTimer.start();
            } else if (cs.phase == 2) {
                cs.phase = 3;
                FragmentManager manager1 = getSupportFragmentManager();
                FragmentTransaction transaction1 = manager1.beginTransaction();
                transaction1.replace(R.id.fragment_container, go);
                transaction1.commit();
                // Hide things from the activity bar
                hideStuffForGameOver();
            }


            gameTimerText.setText("Done");


        }

        public void hideStuffForGameOver() {
            phaseNameText.setVisibility(View.INVISIBLE);
            Button pauseBtn = (Button) findViewById(R.id.pauseGameBtn);
            TextView scoreStaticText = (TextView) findViewById(R.id.scoreStatic);
            scoreStaticText.setVisibility(View.INVISIBLE);
            pauseBtn.setVisibility(View.INVISIBLE);
            scoreText.setVisibility(View.INVISIBLE);
            gameTimerText.setVisibility(View.INVISIBLE);
        }
    }


}


