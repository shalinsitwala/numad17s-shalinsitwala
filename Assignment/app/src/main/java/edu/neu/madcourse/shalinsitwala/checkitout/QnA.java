package edu.neu.madcourse.shalinsitwala.checkitout;


import java.util.ArrayList;

public class QnA {
    String question;
    ArrayList<String> answers;


    public QnA(String question) {
        this.question = question;
    }

    public QnA() {
        this.question = "";
    }

    public QnA(String question, ArrayList<String> answers) {

        this.question = question;
        this.answers = answers;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public ArrayList<String> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<String> answers) {
        this.answers = answers;
    }
}
