package edu.neu.madcourse.shalinsitwala.wordgame;


import android.content.Context;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Random;

import edu.neu.madcourse.shalinsitwala.R;


public class GameFragment extends Fragment {


    ClassicSingleton cs = ClassicSingleton.getInstance();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Retain this fragment across configuration changes.
        // setRetainInstance(true);
        initGame();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView =
                inflater.inflate(R.layout.wglarge_board, container, false);
        initViews(rootView);

        return rootView;
    }


    public void initGame() {
        if (cs.check == null) {
//            cs.mEntireBoard = new Tile(this);
            cs.mEntireBoard = new Tile();
            // Create all the tiles
            for (int large = 0; large < 9; large++) {
                cs.mLargeTiles[large] = new Tile();
                for (int small = 0; small < 9; small++) {
                    cs.mSmallTiles[large][small] = new Tile();
                    cs.mSmallTiles[large][small].setStatus("Blank");
                    cs.mSmallTiles[large][small].setText("");

                }
                cs.mLargeTiles[large].setSubTiles(cs.mSmallTiles[large]);
            }

            cs.mEntireBoard.setSubTiles(cs.mLargeTiles);
        }
    }


    private void initViews(View rootView) {
        final MediaPlayer mediaPlayer = MediaPlayer.create(getActivity(), R.raw.beep_for_word);
        final Vibrator v = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        if (cs.check == null) {
            // this check is for NEW GAME
            // this is new. NOT NULL IS CONTINUE


            cs.mEntireBoard.setView(rootView);


            for (int large = 0; large < 9; large++) {
                final View outer = rootView.findViewById(cs.mLargeIds[large]);
                cs.mLargeTiles[large].setView(outer);

                Random r = new Random();
                int randomPatternNumber = r.nextInt(cs.allPatterns.length);
                Random rand = new Random();
                int randomWordNumber = rand.nextInt(cs.nineWords.length);
                String word = cs.nineWords[randomWordNumber];


                for (int small = 0; small < 9; small++) {

                    final Button inner = (Button) outer.findViewById
                            (cs.mSmallIds[small]);
                    final int fLarge = large;
                    final int fSmall = small;
                    final Tile smallTile = cs.mSmallTiles[large][small];
                    smallTile.setView(inner);

                    //When it loads


                    int pattern[] = cs.allPatterns[randomPatternNumber];
                    int charAt = pattern[small];

                    smallTile.setText(String.valueOf(word.charAt(charAt)));
                    inner.setText(smallTile.getText());


                    // When clicked
                    inner.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            v.vibrate(300);

                            int lastPos = cs.lastClickPosition[fLarge];
                            int currPos = fSmall;
                            if (isClickCompatible(lastPos, currPos)) {

                                cs.lastClickPosition[fLarge] = fSmall;


                                if (smallTile.getStatus() == "Blank") {
                                    //if blank then turn background white
                                    inner.setBackgroundColor(Color.parseColor("#ffffff"));
                                    //change status
                                    smallTile.setStatus("Selected");
                                    //add to array of large grids, to check for each large grid's current word is a word in dict
                                    cs.selectedLetters[fLarge] = cs.selectedLetters[fLarge] + inner.getText();
                                    cs.selectedPositions[fLarge][fSmall] = 1;
                                }

                            }



                            // check if selectedletters[finallarge] is a word in dict
                            boolean isItAWord = isWordInDict(cs.selectedLetters[fLarge]);
                            boolean isItAllowed = allowedWord(cs.selectedLetters[fLarge]);
                            if (isItAWord && isItAllowed) {

                                cs.trueWords[fLarge] = cs.selectedLetters[fLarge];
                                mediaPlayer.seekTo(0);
                                mediaPlayer.start();
                                v.vibrate(1000);


                                // if it is a word, color the selected positions to be cyan.
                                int[] row = cs.selectedPositions[fLarge];
                                for (int i = 0; i < 9; i++) {
                                    if (row[i] == 1) {
                                        Button inner = (Button) outer.findViewById
                                                (cs.mSmallIds[i]);
                                        inner.setBackgroundColor(Color.CYAN);
                                        if (cs.mSmallTiles[fLarge][i].getStatus() == "Selected") {
                                            cs.score += getPoints(cs.mSmallTiles[fLarge][i].getText());
                                        }
                                        cs.mSmallTiles[fLarge][i].setStatus("Word");

                                    }
                                }

                            }
                        }
                    });
                }
            }

            cs.check = "5";

        } else {
            // when continue

            cs.mEntireBoard.setView(rootView);


            for (int large = 0; large < 9; large++) {
                final View outer = rootView.findViewById(cs.mLargeIds[large]);
                cs.mLargeTiles[large].setView(outer);


                for (int small = 0; small < 9; small++) {

                    final Button inner = (Button) outer.findViewById
                            (cs.mSmallIds[small]);
                    final int fLarge = large;
                    final int fSmall = small;
                    final Tile smallTile = cs.mSmallTiles[large][small];
                    smallTile.setView(inner);


                    //When it loads
                    inner.setText(cs.mSmallTiles[fLarge][fSmall].getText());
                    if (cs.mSmallTiles[fLarge][fSmall].getStatus() == "Selected") {
                        inner.setBackgroundColor(Color.parseColor("#ffffff"));
                    }
                    if (cs.mSmallTiles[fLarge][fSmall].getStatus() == "Word") {
                        inner.setBackgroundColor(Color.CYAN);
                    }


                    // When clicked
                    inner.setOnClickListener(new View.OnClickListener() {


                        @Override
                        public void onClick(View view) {
                            v.vibrate(300);
                            int lastPos = cs.lastClickPosition[fLarge];
                            int currPos = fSmall;
                            if (isClickCompatible(lastPos, currPos)) {

                                cs.lastClickPosition[fLarge] = fSmall;


                                if (smallTile.getStatus() == "Blank") {
                                    //if blank then turn background white
                                    inner.setBackgroundColor(Color.parseColor("#ffffff"));
                                    //change status
                                    smallTile.setStatus("Selected");
                                    //add to array of large grids, to check for each large grid's current word is a word in dict
                                    cs.selectedLetters[fLarge] = cs.selectedLetters[fLarge] + inner.getText();
                                    cs.selectedPositions[fLarge][fSmall] = 1;

                                }

                            }


                            // check if selectedletters[finallarge] is a word in dict
                            boolean isItAWord = isWordInDict(cs.selectedLetters[fLarge]);
                            boolean isItAllowed = allowedWord(cs.selectedLetters[fLarge]);

                            if (isItAWord && isItAllowed) {
                                mediaPlayer.seekTo(0);
                                mediaPlayer.start();
                                v.vibrate(1000);


                                // if it is a word, color the selected positions to be cyan.
                                int[] row = cs.selectedPositions[fLarge];
                                for (int i = 0; i < 9; i++) {
                                    if (row[i] == 1) {
                                        Button inner = (Button) outer.findViewById
                                                (cs.mSmallIds[i]);
                                        inner.setBackgroundColor(Color.CYAN);
                                        if (cs.mSmallTiles[fLarge][i].getStatus() == "Selected") {
                                            cs.score += getPoints(cs.mSmallTiles[fLarge][i].getText());
                                        }
                                        cs.mSmallTiles[fLarge][i].setStatus("Word");


                                    }
                                }
                            }
                        }
                    });


                }
            }

        }


    }


    public boolean isClickCompatible(int lastPos, int newPos) {
        if (lastPos == -1) {
            return true;
        }
        if (lastPos == 0) {
            if (newPos == 1 || newPos == 3 || newPos == 4) {
                return true;
            }
        }
        if (lastPos == 1) {
            if (newPos == 0 || newPos == 2 || newPos == 3 || newPos == 4 || newPos == 5) {
                return true;
            }
        }
        if (lastPos == 2) {
            if (newPos == 1 || newPos == 4 || newPos == 5) {
                return true;
            }
        }
        if (lastPos == 3) {
            if (newPos == 0 || newPos == 6 || newPos == 1 || newPos == 4 || newPos == 7) {
                return true;
            }
        }
        if (lastPos == 4) {
            if (newPos >= 0 && newPos <= 8 && newPos != 4) {
                return true;
            }
        }
        if (lastPos == 5) {
            if (newPos == 2 || newPos == 8 || newPos == 1 || newPos == 4 || newPos == 7) {
                return true;
            }
        }
        if (lastPos == 6) {
            if (newPos == 3 || newPos == 4 || newPos == 7) {
                return true;
            }
        }
        if (lastPos == 7) {
            if (newPos == 6 || newPos == 8 || newPos == 3 || newPos == 4 || newPos == 5) {
                return true;
            }
        }
        if (lastPos == 8) {
            if (newPos == 4 || newPos == 5 || newPos == 7) {
                return true;
            }
        }


        return false;
    }


    public boolean allowedWord(String s){
        // checks for word if already selected on any other grid
        for(String tword : cs.trueWords){
            if(s.equalsIgnoreCase(tword)){
                return false;
            }
        }
        return true;
    }



    public boolean isWordInDict(String s) {

        String typed = s;

        typed = typed.toLowerCase();
        if (typed.length() == 3) {
            loadFileWords(typed);
        }

        // handle keywords
        if (typed.equals("con") || typed.equals("for") || typed.equals("int")
                || typed.equals("new") || typed.equals("try")) {
            typed = typed + '1';
            loadFileWords(typed);

            if (typed.contains("1")) {
                typed.replace("1", "");
            }
        }

        if (cs.loaded.contains(typed)) {
            return true;
        }


        return false;
    }

    void loadFileWords(String typed) {
        try {
            BufferedReader buffer = new BufferedReader(
                    new InputStreamReader(getResources().getAssets().open(typed)));

            String str_line = "";

            while ((str_line = buffer.readLine()) != null) {
                str_line = str_line.trim();
                if ((str_line.length() != 0)) {
                    //if those 3 letters file found, then load all the words of that file
                    // in our vector
                    cs.loaded.add(str_line);
                }
            }


        } catch (Exception e) {

        }
    }


    public int getPoints(String s) {
        s = s.toUpperCase();
        int points = 0;

        if (s.equals("E") || s.equals("A") || s.equals("I") || s.equals("O") || s.equals("N") || s.equals("R") || s.equals("T") || s.equals("L") || s.equals("S") || s.equals("U")) {
            points = 1;
        } else if (s.equals("D") || s.equals("G")) {
            points = 2;
        } else if (s.equals("B") || s.equals("C") || s.equals("M") || s.equals("P")) {
            points = 3;
        } else if (s.equals("F") || s.equals("H") || s.equals("V") || s.equals("W") || s.equals("Y")) {
            points = 4;
        } else if (s.equals("K")) {
            points = 5;
        } else if (s.equals("J") || s.equals("X")) {
            points = 8;
        } else if (s.equals("Q") || s.equals("Z")) {
            points = 10;
        }

        return points;
    }


}
