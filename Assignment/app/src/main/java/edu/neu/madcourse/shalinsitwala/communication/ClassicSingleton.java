package edu.neu.madcourse.shalinsitwala.communication;

import java.util.Vector;

public class ClassicSingleton {

    public Vector<User> dbUsers = new Vector<User>();

    public String myToken;
    public String oppToken;

    public String acceptor;
    public String requestor;

    public String SERVER_KEY = "key=AAAAh6WDbO8:APA91bFyv8KyVvapjwjhlOZR3G77IyY4JFEZ8OR_hkkK7MTqCoFuZ7cKiTbU1IVr3f70r_S1-KrP4Cl1RCzakCoKHfjmjQ3KyGoKWsacL-DsM_zhL53hhkCLwOdnqwBOCIQGJqp5WYdv";

    private static ClassicSingleton instance = null;

    private ClassicSingleton() {
        // Exists only to defeat instantiation.
    }

    public static ClassicSingleton getInstance() {
        if (instance == null) {
            instance = new ClassicSingleton();
        }
        return instance;
    }
}