package edu.neu.madcourse.shalinsitwala.twoplayergame;

public class User {
    private String email;
    private String token;
    private String currentGame;
    private String country;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }


    public String getCurrentGame() {
        return currentGame;
    }

    public void setCurrentGame(String currentGame) {
        this.currentGame = currentGame;
    }


    public User(String email, String token, String currentGame, String country) {
        this.email = email;
        this.token = token;
        this.currentGame = currentGame;
        this.country = country;
    }

    public User() {
        this.email = "";
        this.token = "";
        this.currentGame = "";
        this.country = "";
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


}
