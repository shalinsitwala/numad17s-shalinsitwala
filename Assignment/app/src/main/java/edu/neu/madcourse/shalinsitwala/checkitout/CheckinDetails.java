package edu.neu.madcourse.shalinsitwala.checkitout;


import java.util.ArrayList;
import java.util.Date;

public class CheckinDetails {

    String id;
    String locationName;
    String address;
    //For Checkin
    String question;
    String answer;
    ArrayList<String> fanswers;


    //For Places
    ArrayList<QnA> qna;
    Date time;
//    ArrayList<String> tokensOfUsers;


    public ArrayList<String> getFanswers() {
        return fanswers;
    }

    public void setFanswers(ArrayList<String> fanswers) {
        this.fanswers = fanswers;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }


    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }


    public CheckinDetails(String id) {
        this.id = id;
    }

    public CheckinDetails() {
        id = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public ArrayList<QnA> getQna() {
        return qna;
    }

    public void setQna(ArrayList<QnA> qna) {
        this.qna = qna;
    }


}
