package edu.neu.madcourse.shalinsitwala.wordgame;

import android.content.Context;
import android.graphics.Color;
import android.icu.util.TimeUnit;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Random;

import edu.neu.madcourse.shalinsitwala.R;


public class GameFragment2 extends Fragment {

    ClassicSingleton cs = ClassicSingleton.getInstance();


    public String randomVowel() {
        String randomV = "";
        for (int i = 0; i < cs.vowels.length(); i++) {
            Random rand = new Random();
            int randomNum = rand.nextInt(cs.vowels.length());
            randomV = String.valueOf(cs.vowels.charAt(randomNum));
        }
        return randomV;
    }


    // pick randomly any character from selectedletters array from each index
    public void setUpP2Word() {
        for (int i = 0; i < 9; i++) {
            String wordAtThisIndex = cs.selectedLetters[i];

            if (wordAtThisIndex.length() == 0) {
                // if no word selected from the grid, pickup random vowel and add it.
                wordAtThisIndex = randomVowel();
            }

            Random rand = new Random();
            int randomNum = rand.nextInt(wordAtThisIndex.length());
            String randomSelectedLetter = String.valueOf(wordAtThisIndex.charAt(randomNum));
            cs.p2randomword = cs.p2randomword + randomSelectedLetter;
        }
    }


    public String word;


    public GameFragment2() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setUpP2Word();
        word = cs.p2randomword;
        initGame();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView =
                inflater.inflate(R.layout.wgfragment_game_fragment2, container, false);
        initViews(rootView);

        return rootView;

    }


    public void initGame() {
        cs.p2board = new Tile();

        for (int i = 0; i < 9; i++) {
            cs.p2tiles[i] = new Tile();
            cs.p2tiles[i].setStatus("Blank");
            cs.p2tiles[i].setText("");
        }

        cs.p2board.setSubTiles(cs.p2tiles);

    }

    public void initViews(final View rootView) {
        final MediaPlayer mediaPlayer = MediaPlayer.create(getActivity(), R.raw.beep_for_word);

        final Vibrator v = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);


        final TextView p2CurrentWordText = (TextView) rootView.findViewById(R.id.p2currentword);
        p2CurrentWordText.setText(cs.p2selectedletters);
        final TextView scoreText = (TextView) rootView.findViewById(R.id.score);
        cs.p2board.setView(rootView);

        for (int small = 0; small < 9; small++) {
            final Button inner = (Button) rootView.findViewById(cs.p2ids[small]);


            final Tile smallTile = cs.p2tiles[small];
            smallTile.setView(inner);


            // when it loads

            smallTile.setText(String.valueOf(word.charAt(small)));
            inner.setText(smallTile.getText());


            // when clicked
            final int finalSmall = small;
            inner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    v.vibrate(300);

                    if (cs.p2lastclick != finalSmall) { // checks for compatibility
                        cs.p2lastclick = finalSmall;

                        cs.p2selectedletters = cs.p2selectedletters + smallTile.getText();
                        smallTile.setStatus("Selected");
                        inner.setBackgroundColor(Color.WHITE);

                        new CountDownTimer(500, 500) {

                            @Override
                            public void onTick(long arg0) {

                            }

                            @Override
                            public void onFinish() {
                                inner.setBackgroundColor(Color.parseColor("#bfbfbf"));
                            }
                        }.start();

                    }

                    p2CurrentWordText.setText(cs.p2selectedletters);
                    boolean isItAWord = isWordInDict(cs.p2selectedletters);

                    if (isItAWord) {

                        TextView p2CorrectWordText = (TextView)rootView.findViewById(R.id.p2correctword);
                        p2CorrectWordText.setText("Last correct word: " + cs.p2selectedletters);
                        mediaPlayer.seekTo(0);
                        mediaPlayer.start();
                        v.vibrate(1000);

                        for(int i = 0 ; i < 9 ; i++){
                            Tile selTile = cs.p2tiles[i];

                            if(selTile.getStatus().equals("Selected")){
                                cs.score += getPoints(selTile.getText());
                                selTile.setStatus("Word");
                            }


                        }




                    }


                }
            });

        }


    }

    public boolean isWordInDict(String s) {

        String typed = s;

        typed = typed.toLowerCase();
        if (typed.length() == 3) {
            loadFileWords(typed);
        }

        // handle keywords
        if (typed.equals("con") || typed.equals("for") || typed.equals("int")
                || typed.equals("new") || typed.equals("try")) {
            typed = typed + '1';
            loadFileWords(typed);

            if (typed.contains("1")) {
                typed.replace("1", "");
            }
        }

        if (cs.loaded.contains(typed)) {
            return true;
        }


        return false;
    }

    void loadFileWords(String typed) {
        try {
            BufferedReader buffer = new BufferedReader(
                    new InputStreamReader(getResources().getAssets().open(typed)));

            String str_line = "";

            while ((str_line = buffer.readLine()) != null) {
                str_line = str_line.trim();
                if ((str_line.length() != 0)) {
                    //if those 3 letters file found, then load all the words of that file
                    // in our vector
                    cs.loaded.add(str_line);
                }
            }


        } catch (Exception e) {
            System.out.println("Error in reading file.");
        }
    }

    public int getPoints(String s) {
        s = s.toUpperCase();
        int points = 0;
        int bonus = 3;

        if (s.equals("E") || s.equals("A") || s.equals("I") || s.equals("O") || s.equals("N") || s.equals("R") || s.equals("T") || s.equals("L") || s.equals("S") || s.equals("U")) {
            points = 1;
        }
        else if (s.equals("D") || s.equals("G")) {
            points = 2;
        }
        else if (s.equals("B") || s.equals("C") || s.equals("M") || s.equals("P")) {
            points = 3;
        }
        else if (s.equals("F") || s.equals("H") || s.equals("V") || s.equals("W") || s.equals("Y")) {
            points = 4;
        }
        else if (s.equals("K")) {
            points = 5;
        }
        else if (s.equals("J") || s.equals("X")) {
            points = 8;
        }
        else if (s.equals("Q") || s.equals("Z")) {
            points = 10;
        }

        return points * bonus;
    }


}
