package edu.neu.madcourse.shalinsitwala.checkitout;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import edu.neu.madcourse.shalinsitwala.R;

public class tutorial_mycheckins extends AppCompatActivity {

    Button btn_myCheckins;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cio_activity_tutorial_mycheckins);

        // hiding bar
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.hide();
        }


        btn_myCheckins = (Button) findViewById(R.id.cio_mycheckins);
        btn_myCheckins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), MyCheckins.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
