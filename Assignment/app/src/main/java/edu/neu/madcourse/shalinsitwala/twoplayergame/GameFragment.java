package edu.neu.madcourse.shalinsitwala.twoplayergame;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.media.session.MediaSessionCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;


import edu.neu.madcourse.shalinsitwala.R;


public class GameFragment extends Fragment {
    public BroadcastReceiver broadcastReceiver;

    ClassicSingleton cs = ClassicSingleton.getInstance();


    String requestor;
    String acceptor;
    String req, acc;
    DatabaseReference mRoofRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference gameRef = mRoofRef.child("games");
    Game thisGame = new Game();
    String thisGameKey = "";
    String token;
    DatabaseReference gameReference;
    MyTile[][] myTiles = new MyTile[9][9];
    String gameKey;
    int[][] selectedPositions = new int[9][9];
    String sendTo = "";


    LinearLayout noInternetLayout;
    RelativeLayout internetLayout;
    TextView tvGameTimer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Retain this fragment across configuration changes.
        // setRetainInstance(true);

//        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
//        String gameKey = preferences.getString("gameKey", null);
        //        requestor = preferences.getString("requestor", null);

        // Notification data background for acceptor
//        if (gameKey == null || gameKey == "") {
//            gameKey = cs.gameKey;
//        }

        checkInternetConnectivity();

        gameReference = gameRef.child(cs.gameKey);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.twop_fragment_game, container, false);

        internetLayout = (RelativeLayout) rootView.findViewById(R.id.internetLayout);
        noInternetLayout = (LinearLayout) rootView.findViewById(R.id.noInternetLayout);
        tvGameTimer = (TextView) getActivity().findViewById(R.id.gameTimer);
        internetLayout.setVisibility(View.VISIBLE);
        noInternetLayout.setVisibility(View.GONE);

        gameReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot ds) {

                thisGame = ds.getValue(Game.class);
                token = FirebaseInstanceId.getInstance().getToken();
                req = thisGame.getRequestor();
                acc = thisGame.getAcceptor();
                if (req != null) {
                    requestor = req;
                }
                if (acc != null) {
                    acceptor = acc;
                }

                if (token.equalsIgnoreCase(thisGame.getRequestor())) {
                    cs.thisUser = "P1";
                    cs.oppToken = acceptor;
                } else if (token.equalsIgnoreCase(thisGame.getAcceptor())) {
                    cs.thisUser = "P2";
                    cs.oppToken = requestor;
                }

//                Toast.makeText(getContext(), thisUser, Toast.LENGTH_LONG).show();


                drawGrid(rootView, thisGame);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        initViews(rootView);
        return rootView;

    }

    private void drawGrid(View rootView, final Game thisGame) {
        final MediaPlayer mediaPlayer = MediaPlayer.create(getActivity(), R.raw.beep_for_word);
        final Vibrator v = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);


        cs.mEntireBoard.setView(rootView);
        for (int large = 0; large < 9; large++) {
            final View outer = rootView.findViewById(cs.mLargeIds[large]);
            cs.mLargeTiles[large].setView(outer);

            for (int small = 0; small < 9; small++) {
                final Button inner = (Button) outer.findViewById(cs.mSmallIds[small]);
                final int fLarge = large;
                final int fSmall = small;
                final Tile smallTile = cs.mSmallTiles[large][small];
                MyTile myTile = new MyTile();

                if (thisGame.getmSmallTiles().size() != 0) {
                    myTile = thisGame.getmSmallTiles().get(large).get(small);
                }


                cs.rscore = thisGame.getRscore();
                cs.ascore = thisGame.getAscore();

                smallTile.setView(inner);

                // When it loads
                inner.setText(myTile.getText());
                if (myTile.getStatus().equalsIgnoreCase("Selected")) {
                    inner.setBackgroundColor(Color.parseColor("#ffffff"));
                }
                if (myTile.getStatus().equalsIgnoreCase("Word")) {
                    inner.setBackgroundColor(Color.CYAN);
                }


                // When clicked
                final MyTile finalMyTile = myTile;
                inner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        v.vibrate(300);

                        int lastPos = thisGame.getLastClickPosition().get(fLarge);
//                        int lastPos = cs.lastClickPosition[fLarge];
                        int currPos = fSmall;
                        if (isClickCompatible(lastPos, currPos)) {

                            cs.lastClickPosition[fLarge] = fSmall;
                            gameReference.child("lastClickPosition").child(String.valueOf(fLarge)).setValue(fSmall);

//                            if (smallTile.getStatus() == "Blank") {
                            if (finalMyTile.getStatus().equalsIgnoreCase("Blank")) {
                                //if blank then turn background white
                                inner.setBackgroundColor(Color.parseColor("#ffffff"));
                                //change status
                                smallTile.setStatus("Selected");

                                gameReference.child("mSmallTiles").child(String.valueOf(fLarge)).child(String.valueOf(fSmall)).child("status").setValue("Selected");
                                //add to array of large grids, to check for each large grid's current word is a word in dict
                                cs.selectedLetters[fLarge] = cs.selectedLetters[fLarge] + inner.getText();
                                String temp = thisGame.getSelectedLetters().get(fLarge) + thisGame.getmSmallTiles().get(fLarge).get(fSmall).getText();
                                gameReference.child("selectedLetters").child(String.valueOf(fLarge)).setValue(temp);

                                cs.selectedPositions[fLarge][fSmall] = 1;

                                gameReference.child("selectedPositions").child(String.valueOf(fLarge)).child(String.valueOf(fSmall)).setValue(1);


                            }

                        }


                        // check if selectedletters[finallarge] is a word in dict
//                        boolean isItAWord = isWordInDict(cs.selectedLetters[fLarge]);
                        boolean isItAWord = isWordInDict(thisGame.getSelectedLetters().get(fLarge));
//                        boolean isItAllowed = allowedWord(cs.selectedLetters[fLarge]);
                        boolean isItAllowed = allowedWord(thisGame.getSelectedLetters().get(fLarge));
                        if (isItAWord && isItAllowed) {
                            //send notification here to opponent
                            new Thread(new Runnable() {
                                @Override
                                public void run() {

                                    pushNotification("Opponent just found a word!");
                                }
                            }).start();

//                            cs.trueWords[fLarge] = cs.selectedLetters[fLarge];
                            cs.trueWords[fLarge] = thisGame.getSelectedLetters().get(fLarge);
                            gameReference.child("trueWords").child(String.valueOf(fLarge)).setValue(cs.trueWords[fLarge]);
                            mediaPlayer.seekTo(0);
                            mediaPlayer.start();
                            v.vibrate(1000);


                            // if it is a word, color the selected positions to be cyan.
//                            int[] row = cs.selectedPositions[fLarge];
                            int[] row = new int[thisGame.getSelectedPositions().get(fLarge).size()];
                            for (int i = 0; i < row.length; i++) {
                                row[i] = thisGame.getSelectedPositions().get(fLarge).get(i);
                            }

                            for (int i = 0; i < 9; i++) {
                                if (row[i] == 1) {
                                    Button inner = (Button) outer.findViewById(cs.mSmallIds[i]);
                                    inner.setBackgroundColor(Color.CYAN);

//                                    if (cs.mSmallTiles[fLarge][i].getStatus() == "Selected") {
                                    if (thisGame.getmSmallTiles().get(fLarge).get(i).getStatus().equalsIgnoreCase("Selected")) {
//                                        cs.score += getPoints(cs.mSmallTiles[fLarge][i].getText());

                                        if (cs.thisUser.equalsIgnoreCase("P1")) {
                                            cs.rscore += getPoints(thisGame.getmSmallTiles().get(fLarge).get(i).getText());
                                            gameReference.child("rscore").setValue(cs.rscore);
                                        }
                                        if (cs.thisUser.equalsIgnoreCase("P2")) {
                                            cs.ascore += getPoints(thisGame.getmSmallTiles().get(fLarge).get(i).getText());
                                            gameReference.child("ascore").setValue(cs.ascore);
                                        }

                                        cs.score += getPoints(thisGame.getmSmallTiles().get(fLarge).get(i).getText());
                                    }
                                    cs.mSmallTiles[fLarge][i].setStatus("Word");
//                                    MyTile mt = new MyTile();
//                                    mt = thisGame.getmSmallTiles().get(fLarge).get(i);
//                                    mt.setStatus("Word");
                                    gameReference.child("mSmallTiles").child(String.valueOf(fLarge)).child(String.valueOf(i)).child("status").setValue("Word");

                                }
                            }

                        }
                    }
                });


            }
        }
    }

    private void initViews(final View rootView) {

        GameActivity activity = (GameActivity) getActivity();


        // BASIC EMPTY BOARD
        createEmptyGrid();
        // SEND TO DATABASE
//        sendAllTilesToDB(myTiles);


        gameReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot ds) {
                thisGame = ds.getValue(Game.class);
                if (thisGame.getTilesPopulated() == 0) {
                    // POPULATE WORDS IN THE GRID & SEND TO DB
                    populateRandomWords(rootView, thisGame);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });


    }


    private void sendAllTilesToDB(MyTile[][] myTiles) {
        List<List<MyTile>> list = new ArrayList<>();
        for (MyTile[] m : myTiles) {
            list.add(Arrays.asList(m));
        }
        gameReference.child("mSmallTiles").setValue(list);
    }

    private void populateRandomWords(View rootView, final Game thisGame) {
        final MediaPlayer mediaPlayer = MediaPlayer.create(getActivity(), R.raw.beep_for_word);
        final Vibrator v = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        cs.mEntireBoard.setView(rootView);
        for (int large = 0; large < 9; large++) {
            final View outer = rootView.findViewById(cs.mLargeIds[large]);
            cs.mLargeTiles[large].setView(outer);

            Random r = new Random();
            int randomPatternNumber = r.nextInt(cs.allPatterns.length);
            Random rand = new Random();
            int randomWordNumber = rand.nextInt(cs.nineWords.length);
            String word = cs.nineWords[randomWordNumber];


            for (int small = 0; small < 9; small++) {

                final Button inner = (Button) outer.findViewById(cs.mSmallIds[small]);
                final int fLarge = large;
                final int fSmall = small;
                final Tile smallTile = cs.mSmallTiles[large][small];
                smallTile.setView(inner);

                //When it loads
                int pattern[] = cs.allPatterns[randomPatternNumber];
                int charAt = pattern[small];

                smallTile.setText(String.valueOf(word.charAt(charAt)));
                inner.setText(smallTile.getText());
                //              gameReference.child("mSmallTiles").child(String.valueOf(large)).child(String.valueOf(small)).child("text").setValue(String.valueOf(word.charAt(charAt)));
                myTiles[large][small].setText(String.valueOf(word.charAt(charAt)));

                gameReference.child("tilesPopulated").setValue(200);
                // WHEN CLICKED
                inner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        v.vibrate(300);

//                        int lastPos = cs.lastClickPosition[fLarge];
                        int lastPos = thisGame.getLastClickPosition().get(fLarge);
                        int currPos = fSmall;
                        if (isClickCompatible(lastPos, currPos)) {

                            cs.lastClickPosition[fLarge] = fSmall;

                            if (smallTile.getStatus().equalsIgnoreCase("Blank")) {

//                            if (smallTile.getStatus() == "Blank") {
                                //if blank then turn background white
                                inner.setBackgroundColor(Color.parseColor("#ffffff"));
                                //change status
                                smallTile.setStatus("Selected");
                                myTiles[fLarge][fSmall].setStatus("Selected");

                                //add to array of large grids, to check for each large grid's current word is a word in dict
                                cs.selectedLetters[fLarge] = cs.selectedLetters[fLarge] + inner.getText();
                                cs.selectedPositions[fLarge][fSmall] = 1;
                            }

                        }
                        sendAllTilesToDB(myTiles);


                        // check if selectedletters[finallarge] is a word in dict
                        boolean isItAWord = isWordInDict(cs.selectedLetters[fLarge]);
                        boolean isItAllowed = allowedWord(cs.selectedLetters[fLarge]);
                        if (isItAWord && isItAllowed) {

                            cs.trueWords[fLarge] = cs.selectedLetters[fLarge];
                            mediaPlayer.seekTo(0);
                            mediaPlayer.start();
                            v.vibrate(1000);


                            // if it is a word, color the selected positions to be cyan.
                            int[] row = cs.selectedPositions[fLarge];
                            for (int i = 0; i < 9; i++) {
                                if (row[i] == 1) {
                                    Button inner = (Button) outer.findViewById(cs.mSmallIds[i]);
                                    inner.setBackgroundColor(Color.CYAN);

                                    if (cs.mSmallTiles[fLarge][i].getStatus().equalsIgnoreCase("Selected")) {
                                        cs.score += getPoints(cs.mSmallTiles[fLarge][i].getText());
                                    }
                                    cs.mSmallTiles[fLarge][i].setStatus("Word");
                                    myTiles[fLarge][i].setStatus("Word");

                                }
                            }
                            sendAllTilesToDB(myTiles);

                        }
                    }
                });

            }
        }

        List<List<Integer>> iList = new ArrayList<>();
        for (int[] i : selectedPositions) {
            List<Integer> intList = new ArrayList<Integer>();
            for (int index = 0; index < i.length; index++) {
                intList.add(i[index]);
            }
            iList.add(intList);
        }
        gameReference.child("selectedPositions").setValue(iList);


        List<List<MyTile>> list = new ArrayList<>();
        for (MyTile[] m : myTiles) {
            list.add(Arrays.asList(m));
        }
        gameReference.child("mSmallTiles").setValue(list);

    }


    private void createEmptyGrid() {
        cs.mEntireBoard = new Tile();
        // Create all the tiles
        for (int large = 0; large < 9; large++) {
            cs.mLargeTiles[large] = new Tile();
            for (int small = 0; small < 9; small++) {
                cs.mSmallTiles[large][small] = new Tile();
                cs.mSmallTiles[large][small].setStatus("Blank");
                cs.mSmallTiles[large][small].setText("");
                myTiles[large][small] = new MyTile();
            }
            cs.mLargeTiles[large].setSubTiles(cs.mSmallTiles[large]);
        }
        cs.mEntireBoard.setSubTiles(cs.mLargeTiles);
    }


    private Game findGameByReqOrAcc(DataSnapshot ds) {
        for (DataSnapshot d : ds.getChildren()) {
            String rq = d.getValue(Game.class).getRequestor();
            String ac = d.getValue(Game.class).getAcceptor();
            if (rq.equalsIgnoreCase(cs.requestor) || ac.equalsIgnoreCase(cs.acceptor)) {
                cs.thisGame = d.getValue(Game.class);
                thisGame = cs.thisGame;
                thisGameKey = d.getKey();
                //displayTokens.setText("req is " + thisGame.getRequestor() + " acceptor is " + thisGame.getAcceptor() + " current score is " + thisGame.getRscore() + thisGame.getAscore());
            }
        }
        return thisGame;
    }


    public boolean isClickCompatible(int lastPos, int newPos) {
        if (lastPos == -1) {
            return true;
        }
        if (lastPos == 0) {
            if (newPos == 1 || newPos == 3 || newPos == 4) {
                return true;
            }
        }
        if (lastPos == 1) {
            if (newPos == 0 || newPos == 2 || newPos == 3 || newPos == 4 || newPos == 5) {
                return true;
            }
        }
        if (lastPos == 2) {
            if (newPos == 1 || newPos == 4 || newPos == 5) {
                return true;
            }
        }
        if (lastPos == 3) {
            if (newPos == 0 || newPos == 6 || newPos == 1 || newPos == 4 || newPos == 7) {
                return true;
            }
        }
        if (lastPos == 4) {
            if (newPos >= 0 && newPos <= 8 && newPos != 4) {
                return true;
            }
        }
        if (lastPos == 5) {
            if (newPos == 2 || newPos == 8 || newPos == 1 || newPos == 4 || newPos == 7) {
                return true;
            }
        }
        if (lastPos == 6) {
            if (newPos == 3 || newPos == 4 || newPos == 7) {
                return true;
            }
        }
        if (lastPos == 7) {
            if (newPos == 6 || newPos == 8 || newPos == 3 || newPos == 4 || newPos == 5) {
                return true;
            }
        }
        if (lastPos == 8) {
            if (newPos == 4 || newPos == 5 || newPos == 7) {
                return true;
            }
        }


        return false;
    }


    public boolean allowedWord(String s) {
        // checks for word if already selected on any other grid
        for (String tword : thisGame.getTrueWords()) {
            if (s.equalsIgnoreCase(tword)) {
                return false;
            }
        }
        return true;
    }


    public boolean isWordInDict(String s) {

        String typed = s;

        typed = typed.toLowerCase();
        if (typed.length() == 3) {
            loadFileWords(typed);
        }

        // handle keywords
        if (typed.equals("con") || typed.equals("for") || typed.equals("int")
                || typed.equals("new") || typed.equals("try")) {
            typed = typed + '1';
            loadFileWords(typed);

            if (typed.contains("1")) {
                typed.replace("1", "");
            }
        }

        if (cs.loaded.contains(typed)) {
            return true;
        }


        return false;
    }

    void loadFileWords(String typed) {
        try {
            BufferedReader buffer = new BufferedReader(
                    new InputStreamReader(getResources().getAssets().open(typed)));

            String str_line = "";

            while ((str_line = buffer.readLine()) != null) {
                str_line = str_line.trim();
                if ((str_line.length() != 0)) {
                    //if those 3 letters file found, then load all the words of that file
                    // in our vector
                    cs.loaded.add(str_line);
                }
            }


        } catch (Exception e) {

        }
    }


    public int getPoints(String s) {
        s = s.toUpperCase();
        int points = 0;

        if (s.equals("E") || s.equals("A") || s.equals("I") || s.equals("O") || s.equals("N") || s.equals("R") || s.equals("T") || s.equals("L") || s.equals("S") || s.equals("U")) {
            points = 1;
        } else if (s.equals("D") || s.equals("G")) {
            points = 2;
        } else if (s.equals("B") || s.equals("C") || s.equals("M") || s.equals("P")) {
            points = 3;
        } else if (s.equals("F") || s.equals("H") || s.equals("V") || s.equals("W") || s.equals("Y")) {
            points = 4;
        } else if (s.equals("K")) {
            points = 5;
        } else if (s.equals("J") || s.equals("X")) {
            points = 8;
        } else if (s.equals("Q") || s.equals("Z")) {
            points = 10;
        }

        return points;
    }


    public GameFragment() {
        // Required empty public constructor
    }


    private void pushNotification(String body) {
        JSONObject jPayload = new JSONObject();
        JSONObject jNotification = new JSONObject();
        JSONObject jData = new JSONObject();
        try {
            // this content is when app is in background
            jNotification.put("title", "Scroggle!");
            jNotification.put("body", body);
            jNotification.put("sound", "default");
            jNotification.put("badge", "1");
            jNotification.put("click_action", "TWOPGAME");

            jData.put("gameKey", gameKey);

//            if (cs.thisUser.equalsIgnoreCase("P1")) {
//                sendTo = cs.acceptor;
//            } else if (cs.thisUser.equalsIgnoreCase("P2")) {
//                sendTo = cs.requestor;
//            }

            sendTo = cs.oppToken;


            jPayload.put("to", sendTo);


            jPayload.put("priority", "high");
            jPayload.put("notification", jNotification);
            jPayload.put("data", jData);

            URL url = new URL("https://fcm.googleapis.com/fcm/send");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", cs.SERVER_KEY);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);

            // Send FCM message content.
            OutputStream outputStream = conn.getOutputStream();
            outputStream.write(jPayload.toString().getBytes());
            outputStream.close();

            // Read FCM response.
            InputStream inputStream = conn.getInputStream();
            final String resp = convertStreamToString(inputStream);

//            Handler h = new Handler(Looper.getMainLooper());
//            h.post(new Runnable() {
//                @Override
//                public void run() {
//                    Toast.makeText(getContext(), resp, Toast.LENGTH_LONG);
//                }
//            });
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    private String convertStreamToString(InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next().replace(",", ",\n") : "";
    }


    public void internetGone() {
        internetLayout.setVisibility(View.GONE);
        noInternetLayout.setVisibility(View.VISIBLE);

        //send notification here to opponent
        new Thread(new Runnable() {
            @Override
            public void run() {

                pushNotification("Opponent has disconnected from the game.");
            }
        }).start();
    }


    private void checkInternetConnectivity() {
        IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int[] type = {ConnectivityManager.TYPE_WIFI, ConnectivityManager.TYPE_MOBILE};
                if (CheckInternetBroadcast.isNetworkAvailable(context, type)) {
//                    internetFound();
                    internetLayout.setVisibility(View.VISIBLE);
                    tvGameTimer.setVisibility(View.VISIBLE);
                    noInternetLayout.setVisibility(View.GONE);
//                    Toast.makeText(getContext(), "Internet found", Toast.LENGTH_LONG).show();
                } else {
//                    internetGone();
                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            pushNotification("Opponent has disconnected from the game.");
                        }
                    }).start();
                    noInternetLayout.setVisibility(View.VISIBLE);
                    internetLayout.setVisibility(View.GONE);
                    tvGameTimer.setVisibility(View.GONE);
//                    Toast.makeText(getContext(), "Internet gone", Toast.LENGTH_LONG).show();
                }
            }
        };

        //register receiver
        getActivity().registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onPause() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                pushNotification("Opponent has disconnected from the game.");
            }
        }).start();
        super.onPause();


    }


}
