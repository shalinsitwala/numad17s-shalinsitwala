package edu.neu.madcourse.shalinsitwala.checkitout;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import edu.neu.madcourse.shalinsitwala.R;

public class Register extends AppCompatActivity {
    DatabaseReference mRoofRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference personsRef = mRoofRef.child("persons");
    DatabaseReference thisPersonRef;
    DatabaseReference placesRef = mRoofRef.child("places");
    String token = "";
    Person thisPerson;


    EditText et_email;
    ImageButton btn_next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cio_activity_register);
        setTitle("Register");


        et_email = (EditText) findViewById(R.id.et_email);
        btn_next = (ImageButton) findViewById(R.id.next_button);

        token = FirebaseInstanceId.getInstance().getToken();
        thisPersonRef = personsRef.child(token);

        thisPersonRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot ds) {
                thisPerson = ds.getValue(Person.class);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String typedEmail = et_email.getText().toString();

                if (isValidEmail(et_email.getText())) {
                    thisPersonRef.child("email").setValue(typedEmail);
                    Intent intent = new Intent(getBaseContext(), RegisterComplete.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(getBaseContext(), "Please enter a valid email address", Toast.LENGTH_LONG).show();
                }


            }
        });
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
}
