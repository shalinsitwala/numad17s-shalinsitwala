package edu.neu.madcourse.shalinsitwala.communication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.provider.ContactsContract;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import edu.neu.madcourse.shalinsitwala.R;

public class GamePlay extends AppCompatActivity {
    private BroadcastReceiver broadcastReceiver;

    ClassicSingleton cs = ClassicSingleton.getInstance();
    String requestor = "";
    String acceptor = "";
    String typedWord = "";
    String sendTo = "";
    TextView displayTokens, tvP1Score, tvP2Score;
    ListView showGameWords;
    Button btnSubmitWord;
    EditText enterWord;
    LinearLayout noInternetLayout;
    RelativeLayout internetLayout;
    TextView tvNoInternetMessage;

    DatabaseReference mRoofRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference gameRef = mRoofRef.child("games");
    DatabaseReference thisGameRef;
    Game thisGame = new Game();
    String thisGameKey = "";
    String token, thisUser;
    ArrayList<String> words = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.com_activity_game_play);

        internetLayout = (RelativeLayout) findViewById(R.id.internetLayout);
        noInternetLayout = (LinearLayout) findViewById(R.id.noInternetLayout);
        tvNoInternetMessage = (TextView) findViewById(R.id.tvNoInternetMessage);
        // hiding bar
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.hide();
        }
        Intent startingIntent = getIntent();
        checkInternetConnectivity();
        requestor = startingIntent.getStringExtra("requestor");
        acceptor = startingIntent.getStringExtra("acceptor");

        displayTokens = (TextView) findViewById(R.id.textView10);
        showGameWords = (ListView) findViewById(R.id.showGameWords);
        btnSubmitWord = (Button) findViewById(R.id.btnSubmitWord);
        enterWord = (EditText) findViewById(R.id.enterWord);
        tvP1Score = (TextView) findViewById(R.id.tvP1Score);
        tvP2Score = (TextView) findViewById(R.id.tvP2Score);


        noInternetLayout.setVisibility(View.GONE);

        internetFound();
    }


    public void internetFound() {

        internetLayout.setVisibility(View.VISIBLE);
        noInternetLayout.setVisibility(View.GONE);


        gameRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                findGameByReqOrAcc(dataSnapshot);

                cs.requestor = thisGame.getRequestor();
                cs.acceptor = thisGame.getAcceptor();

                token = FirebaseInstanceId.getInstance().getToken();
                if (token.equalsIgnoreCase(thisGame.getAcceptor())) {
                    thisUser = "P2";
                } else if (token.equalsIgnoreCase(thisGame.getRequestor())) {
                    thisUser = "P1";
                }


                displayTokens.setText("You are Player " + thisUser);

                tvP1Score.setText("P1 Score: " + String.valueOf(thisGame.getRscore()));
                tvP2Score.setText("P2 Score: " + String.valueOf(thisGame.getAscore()));


                words = thisGame.getWords();
                thisGameRef = gameRef.child(thisGameKey);

                if (words.size() > 0) {
                    ArrayAdapter adapter = new ArrayAdapter(GamePlay.this, android.R.layout.simple_list_item_1, words);
                    showGameWords.setAdapter(adapter);


                } else {
                    //No one has typed anything yet.
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        btnSubmitWord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //push word to list in the object
                typedWord = enterWord.getText().toString();
                String currentWord = thisUser + ": " + enterWord.getText().toString();

                thisGame.getWords().add(currentWord); // not sure if it is inserted or no


                //update scores here
                if (thisUser.equalsIgnoreCase("P1")) {
                    int newscore = thisGame.getRscore() + enterWord.getText().toString().length();
                    thisGame.setRscore(newscore);
                } else if (thisUser.equalsIgnoreCase("P2")) {
                    int newscore = thisGame.getAscore() + enterWord.getText().toString().length();
                    thisGame.setAscore(newscore);
                }
                thisGame.setTime(new Date());


                // send notification to the opp user here
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        pushNotification("Opponent made a move of: " + typedWord);
                    }
                }).start();


                enterWord.setText("");

                // push the object to db.
                thisGameRef.setValue(thisGame);


            }
        });
    }

    private void pushNotification(String body) {
        JSONObject jPayload = new JSONObject();
        JSONObject jNotification = new JSONObject();
        JSONObject jData = new JSONObject();
        try {
            // this content is when app is in background
            jNotification.put("title", "Scroggle!");
            jNotification.put("body", body);
            jNotification.put("sound", "default");
            jNotification.put("badge", "1");
            jNotification.put("click_action", "GAMEPLAY");

            jData.put("requestor", token);


            if (thisUser.equalsIgnoreCase("P1")) {
                sendTo = cs.acceptor;
            } else if (thisUser.equalsIgnoreCase("P2")) {
                sendTo = cs.requestor;
            }


            jPayload.put("to", sendTo);


            jPayload.put("priority", "high");
            jPayload.put("notification", jNotification);
            jPayload.put("data", jData);

            URL url = new URL("https://fcm.googleapis.com/fcm/send");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", cs.SERVER_KEY);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);

            // Send FCM message content.
            OutputStream outputStream = conn.getOutputStream();
            outputStream.write(jPayload.toString().getBytes());
            outputStream.close();

            // Read FCM response.
            InputStream inputStream = conn.getInputStream();
            final String resp = convertStreamToString(inputStream);

            Handler h = new Handler(Looper.getMainLooper());
            h.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getBaseContext(), resp, Toast.LENGTH_LONG);
                }
            });
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    private String convertStreamToString(InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next().replace(",", ",\n") : "";
    }


    private void findGameByReqOrAcc(DataSnapshot ds) {
        for (DataSnapshot d : ds.getChildren()) {
            String rq = d.getValue(Game.class).getRequestor();
            String ac = d.getValue(Game.class).getAcceptor();
            if (rq.equalsIgnoreCase(requestor) || ac.equalsIgnoreCase(acceptor)) {
                thisGame = d.getValue(Game.class);
                thisGameKey = d.getKey();
                //displayTokens.setText("req is " + thisGame.getRequestor() + " acceptor is " + thisGame.getAcceptor() + " current score is " + thisGame.getRscore() + thisGame.getAscore());
            }
        }
    }


    public void internetGone() {
        internetLayout.setVisibility(View.GONE);
        noInternetLayout.setVisibility(View.VISIBLE);

        //send notification here to opponent
        new Thread(new Runnable() {
            @Override
            public void run() {

                pushNotification("Opponent has disconnected from the game.");
            }
        }).start();
    }

    private void checkInternetConnectivity() {
        IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int[] type = {ConnectivityManager.TYPE_WIFI, ConnectivityManager.TYPE_MOBILE};
                if (CheckInternetBroadcast.isNetworkAvailable(context, type)) {
                    internetFound();
                } else {
                    internetGone();

                }
            }
        };

        //register receiver
        registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }


}
