package edu.neu.madcourse.shalinsitwala.checkitout;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.Locale;

import edu.neu.madcourse.shalinsitwala.R;

public class LandingPage extends AppCompatActivity {
    public BroadcastReceiver broadcastReceiver;
    ClassicSingleton csProject = ClassicSingleton.getInstance();
    String token;
    DatabaseReference mDataBaseRef;
    DatabaseReference personsRef;
    Geocoder geocoder;
    LocationListener locationListener;
    LocationManager locationManager;


    TextView tv_internet;
    ProgressBar pb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cio_activity_landing_page);


        checkInternetConnectivity();


        // hiding bar
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.hide();
        }

        tv_internet = (TextView) findViewById(R.id.tv_internet_message);
        pb = (ProgressBar) findViewById(R.id.progressBar3);

        ////////// LOCATION STARTS///////////
        geocoder = new Geocoder(this, Locale.getDefault());
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.

                csProject.lat = location.getLatitude();
                csProject.lon = location.getLongitude();

            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
//                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                startActivity(i);
            }
        };
        configure_button();
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);


        //////////// LOCATION ENDS //////////////


        mDataBaseRef = FirebaseDatabase.getInstance().getReference();
        personsRef = mDataBaseRef.child("persons");
        token = FirebaseInstanceId.getInstance().getToken();


        personsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot ds) {
                if (ds.hasChild(token)) {
                    // User already there

                    Intent checkinPageIntent = new Intent(getBaseContext(), CheckIn.class);
                    startActivity(checkinPageIntent);

                    finish();
//                    Toast.makeText(getBaseContext(), "User already there", Toast.LENGTH_LONG).show();

                } else {
                    // New User
                    Person person = new Person(token);


                    // Add to DB
                    personsRef.child(token).setValue(person);

                    Intent welcomePageIntent = new Intent(getBaseContext(), Home.class);
                    startActivity(welcomePageIntent);
                    finish();
//                    Toast.makeText(getBaseContext(), "User added to DB", Toast.LENGTH_LONG).show();

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

    private void checkInternetConnectivity() {
        IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int[] type = {ConnectivityManager.TYPE_WIFI, ConnectivityManager.TYPE_MOBILE};
                if (CheckInternetBroadcast.isNetworkAvailable(context, type)) {
//                    internetFound();

                    tv_internet.setVisibility(View.INVISIBLE);
                    pb.setVisibility(View.VISIBLE);
                } else {
//                    internetGone();
                    tv_internet.setVisibility(View.VISIBLE);
                    pb.setVisibility(View.INVISIBLE);
                }
            }
        };

        //register receiver
        getBaseContext().registerReceiver(broadcastReceiver, intentFilter);
    }


    void configure_button() {
        // first check for permissions
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.INTERNET}
                        , 10);
            }
            return;
        }
        // this code won't execute IF permissions are not allowed, because in the line above there is return statement.
        locationManager.requestLocationUpdates("gps", 5000, 0, locationListener);
    }
}
