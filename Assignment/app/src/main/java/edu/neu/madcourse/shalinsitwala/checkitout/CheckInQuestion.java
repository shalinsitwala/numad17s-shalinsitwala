package edu.neu.madcourse.shalinsitwala.checkitout;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ButtonBarLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Set;

import edu.neu.madcourse.shalinsitwala.R;

public class CheckInQuestion extends AppCompatActivity {

    ClassicSingleton cs = ClassicSingleton.getInstance();

    DatabaseReference mRoofRef = FirebaseDatabase.getInstance().getReference();
    DatabaseReference personsRef = mRoofRef.child("persons");
    DatabaseReference thisPersonRef;
    DatabaseReference placesRef = mRoofRef.child("places");
    DatabaseReference thisPlaceRef;
    DatabaseReference askingToPersonRef;
    String token = "";
    Person thisPerson;


    CheckinDetails checkinDetails;
    ArrayList<String> samplequestions;
    ArrayList<CheckinDetails> checkins;
    ArrayList<String> allTokensInDB = new ArrayList<>();

    Button btn_askyourown;
    int checkInNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cio_activity_check_in_question);

        // hiding bar
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.hide();
        }


        btn_askyourown = (Button) findViewById(R.id.btn_askyourown);
        getAllTokensOfDB();

        samplequestions = new ArrayList<String>();
        samplequestions.add("What are the hours?");
        samplequestions.add("Fun things to do here?");
        samplequestions.add("Where to go next?");
        samplequestions.add("Nearest free parking?");
        samplequestions.add("Best thing here?");


        token = FirebaseInstanceId.getInstance().getToken();
        thisPersonRef = personsRef.child(token);


        thisPersonRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot ds) {
                thisPerson = ds.getValue(Person.class);

                checkins = thisPerson.getCheckins();

                // Most recent checkin
                checkinDetails = checkins.get(checkins.size() - 1);


                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(CheckInQuestion.this, android.R.layout.simple_list_item_1, samplequestions);
                ListView lv = (ListView) findViewById(R.id.presetquestions);
                lv.setAdapter(arrayAdapter);

                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                        //Toast.makeText(getApplicationContext(), "Selected item at position: " + position, Toast.LENGTH_LONG).show();
                        final String selectedQuestion = samplequestions.get(position);


                        // Add this question to this checkin
                        checkinDetails.setQuestion(selectedQuestion);
                        checkins.set(checkins.size() - 1, checkinDetails);
                        thisPersonRef.child("checkins").setValue(checkins);
                        checkInNumber = checkins.size() - 1;

                        // Add this to "Places"
                        thisPlaceRef = placesRef.child(checkinDetails.getId());

                        thisPlaceRef.child("qna").addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot ds) {
                                GenericTypeIndicator<ArrayList<QnA>> t = new GenericTypeIndicator<ArrayList<QnA>>() {
                                };

                                ArrayList<QnA> qna = ds.getValue(t);
                                if (qna == null) {
                                    QnA newQuestion = new QnA(selectedQuestion);

                                    qna = new ArrayList<QnA>();
                                    qna.add(newQuestion);
                                } else {
                                    QnA newQuestion = new QnA(selectedQuestion);
                                    qna.add(newQuestion);
                                }


                                thisPlaceRef.child("qna").setValue(qna);
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                        thisPlaceRef.child("question").setValue(selectedQuestion);


                        // do stuff here
                        sendQuestion(selectedQuestion);

                        if (allTokensInDB != null) {
                            for (final String eachToken : allTokensInDB) {

                                askingToPersonRef = personsRef.child(eachToken);
                                askingToPersonRef.child("askerToken").setValue(token);
                                askingToPersonRef.child("askerCheckin").setValue(checkInNumber);

                                // SEND NOTIFICATION TO FRIEND
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {

                                        pushNotification(thisPerson.getEmail() + " asked a question.", eachToken);
                                    }
                                }).start();

                            }
                        }

                        int points = thisPerson.getPoints();
                        points = points - 15;
                        thisPersonRef.child("points").setValue(points);

                        Intent i = new Intent(getBaseContext(), SendQuestionConfirmation.class);
                        startActivity(i);
                        finish();
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        btn_askyourown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), AskQuestion.class);
                startActivity(intent);
                finish();
            }
        });
    }

    void sendQuestion(String question) {
        // code to send question to friends
    }


    public void getAllTokensOfDB() {
        personsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot ds) {
                for (DataSnapshot d : ds.getChildren()) {
                    Person person = d.getValue(Person.class);
                    if (person.getToken().equalsIgnoreCase(token)) {
                        // don't add this user's id

                    } else {
                        allTokensInDB.add(person.getToken());
//                        Toast.makeText(AskQuestion.this, "Token added" + person.getToken(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private void pushNotification(String body, String to) {
        JSONObject jPayload = new JSONObject();
        JSONObject jNotification = new JSONObject();
        JSONObject jData = new JSONObject();
        try {
            // this content is when app is in background
            jNotification.put("title", "Check It Out");
            jNotification.put("body", body);
            jNotification.put("sound", "default");
            jNotification.put("badge", "1");
            jNotification.put("click_action", "ANSWERPAGE");

            // If sending to a single client
            jPayload.put("to", to);
            jPayload.put("priority", "high");
            jPayload.put("notification", jNotification);
            jPayload.put("data", jData);

            URL url = new URL("https://fcm.googleapis.com/fcm/send");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", cs.SERVER_KEY);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);

            // Send FCM message content.
            OutputStream outputStream = conn.getOutputStream();
            outputStream.write(jPayload.toString().getBytes());
            outputStream.close();

            // Read FCM response.
            InputStream inputStream = conn.getInputStream();
            final String resp = convertStreamToString(inputStream);

            Handler h = new Handler(Looper.getMainLooper());
            h.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getBaseContext(), resp, Toast.LENGTH_LONG);
                }
            });
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    private String convertStreamToString(InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next().replace(",", ",\n") : "";
    }
}
